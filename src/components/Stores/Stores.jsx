import React, {useEffect, useState} from 'react';
import {Button, Container, FormControl, FormHelperText, Grid, InputLabel, MenuItem, Select} from '@material-ui/core';
import useStyles from './styles';
import Product from '../Products/Product/Product';
import {alpha, styled} from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from '@material-ui/icons/Search';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import PdfFile from '../PdfFile/PdfFile.js'
import SendEmail from '../SendEmail/SendEmail';

function Stores({products, handleAddProduct}) {
    const classes = useStyles();
    const [productList, setProductList] = useState(products);
    const [productSeller, setProductSeller] = useState('');
    const [searchValue, setSearchValue] = useState("");
    const [sortPriceDesc, setSortPriceDesc] = useState(false);

    const handleChange = (event) => {
        setProductSeller(event.target.value);
    };

    useEffect(() => {
        if (products) {
            console.log(products);
            setProductList(products);
        }
    }, [products])


    const filteredProducts = products.map((product) =>
        product.Product_Seller).filter(function (value, index, array) {
        return array.indexOf(value) === index;
    });


    const Search = styled('div')(({theme}) => ({
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: alpha(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: alpha(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    }));

    const SearchIconWrapper = styled('div')(({theme}) => ({
        padding: theme.spacing(0, 2),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }));

    const StyledInputBase = styled(InputBase)(({theme}) => ({
        color: 'inherit',
        '& .MuiInputBase-input': {
            padding: theme.spacing(1, 75, 1, 5),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(4)})`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('md')]: {
                width: '50ch',
            },
        },
    }));


    useEffect(() => {
        if (searchValue) {
            const filteredItems = productList.filter(p => p.Product_Name?.toUpperCase().includes(searchValue.toUpperCase()));
            setProductList(filteredItems);
        } else {
            setProductList(products);
        }

    }, [searchValue])

    const searchHandler = (e) => {
        if (e.key === "Enter") {
            setSearchValue(e.target.value);
        }
    };

    const sortByPriceHandler = () => {
        setSortPriceDesc(!sortPriceDesc);
    }

    const soldProducts = [
        {
            "id" :1,
            "productName" : "Rose",
            "price" : 123.4,
            "soldDate" :"05.05.2022",
            "soldCount" : 1
        },
        {
            "id" :2,
            "productName" : "Tulip",
            "price" : 456.3,
            "soldDate" :"06.05.2022",
            "soldCount" : 2
        },
        {
            "id" :3,
            "productName" : "Red Rose",
            "price" : 11.3,
            "soldDate" :"07.05.2022",
            "soldCount" : 3
        }
    ];

    const sendEmailName = "Kemal Guler";
    const sendEmailTo ="kemal.guler@ozu.edu.tr";

    return (
        <Container>


            

            <div className={classes.toolbar}/>
            <div className={classes.combobox}>
                <FormControl variant="standard" className={classes.FormControl}>
                    <InputLabel shrink>Stores</InputLabel>
                    <Select
                        labelId="select-demo"
                        id="demo-simple-select"
                        displayEmpty
                        value={productSeller}
                        onChange={handleChange}
                    >
                        <MenuItem value={'all'}>All</MenuItem>

                        {filteredProducts.map((product, index) => (
                            <MenuItem item key={index} value={product}>{product}</MenuItem>))}
                    </Select>
                    <FormHelperText>You can select Store</FormHelperText>
                </FormControl>
            </div>
            <main className={classes.root}>
                <div className={classes.toolbar}>
                </div>
                {
                  productSeller && <Button style={{float: "right", marginRight: "20%"}}
                                          endIcon={sortPriceDesc ? <ArrowDownwardIcon/> : <ArrowUpwardIcon/>}
                                          onClick={sortByPriceHandler}>
                    Price
                  </Button>
                }
                
                <Grid container justify="center" spacing={4}>
                    {
                        productList && productList?.length > 0 ? (
                            productList?.sort((p1,p2)=>{
                                return sortPriceDesc ? (parseInt(p2.Product_Price)  - parseInt(p1.Product_Price)) :  (parseInt(p1.Product_Price)  - parseInt(p2.Product_Price));
                            }
                            ).map((product) => (
                                <>
                                    {product.Product_Seller === productSeller && (
                                        <Grid item key={product.Product_Seller} xs={12} sm={6} md={3} lg={2}>
                                            <Product product={product} handleAddProduct={handleAddProduct}/>
                                        </Grid>
                                    )}
                                    {productSeller === 'all' && (
                                        <Grid item key={product.Product_Seller} xs={12} sm={6} md={3} lg={2}>
                                            <Product product={product} handleAddProduct={handleAddProduct}/>
                                        </Grid>
                                    )}
                                </>
                            ))
                        ) : (<div>There is no item</div>)
                    }
                </Grid>
            </main>
        </Container>
    );
}

export default Stores;

