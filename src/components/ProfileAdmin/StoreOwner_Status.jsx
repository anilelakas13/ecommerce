import {
  Card,
  CardContent,
  CardActions,
  Typography,
  IconButton,
} from "@material-ui/core";
import ChangeCircleIcon from "@mui/icons-material/ChangeCircle";
import firebase from "../Firebase/fire";
import React, { useState } from "react";
import emailjs from "emailjs-com";

import useStyles from "./styles";

const StoreOwner_Status = ({ store }) => {
  const classes = useStyles();
  const [tabSelection, setTabSelection] = useState([]);

  const changeStoreOwnerStatus = async (store) => {
    const refCustomers = firebase
      .firestore()
      .collection("users")
      .where("email", "==", store.email);

    console.log(store.email);
    if (store.status === "1") {
      refCustomers.get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.update({
            status: "0",
          });
        });
      });

      const refProducts = firebase
        .firestore()
        .collection("Products")
        .where("Product_Seller", "==", store.displayName);
      refProducts.get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.update({
            Product_Status: "0",
          });
        });
      });

      emailjs.init("9qipjvqAhN_zEkeDW");
      emailjs.send("service_1pripqs", "template_8bcnu0g", {
        to_email: store.email,
        to_name: store.displayName,
        general_message: "Your account is suspended.",
      });
    } else if (store.status === "0") {
      refCustomers.get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.update({
            status: "1",
          });
        });
      });
      const refProducts = firebase
        .firestore()
        .collection("Products")
        .where("Product_Seller", "==", store.displayName);
      refProducts.get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.update({
            Product_Status: "1",
          });
        });
      });

      emailjs.init("9qipjvqAhN_zEkeDW");
      emailjs.send("service_1pripqs", "template_8bcnu0g", {
        to_email: store.email,
        to_name: store.displayName,
        general_message: "Your account is unsuspended.",
      });
    }
    setTabSelection(2);
    console.log(store);
  };

  return (
    <Card className={classes.root}>
      <CardContent>
        <div className={classes.cardContent}>
          <Typography
            variant="h5"
            component="h2"
            gutterBottom
            style={{ fontWeight: 550 }}
          >
            Name: {store.displayName}
          </Typography>
          <Typography
            variant="h5"
            component="h2"
            gutterBottom
            style={{ fontWeight: 550 }}
          >
            Phone: {store.phone}
          </Typography>
          <Typography
            variant="h5"
            component="h2"
            gutterBottom
            style={{ fontWeight: 550 }}
          >
            E-mail: {store.email}
          </Typography>
        </div>
        <Typography
          variant="body2"
          color="textSecondary"
          component="p"
          gutterBottom
        >
          Store Status:{" "}
          {store.status == "1" ? <div>Unsuspend</div> : <div>Suspend</div>}
        </Typography>
      </CardContent>
      <CardActions disableSpacing className={classes.cardActions}>
        <IconButton
          aria-label="Change Status"
          onClick={(e) => {
            console.log(store);
            changeStoreOwnerStatus(store);
          }}
        >
          <ChangeCircleIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default StoreOwner_Status;
