import { useAdminAuth } from './../CustomHooks/index.js';

const WithAdminAuth = props => useAdminAuth(props) && props.children;

export default WithAdminAuth;