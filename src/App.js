import React, { useState, useEffect } from 'react';

import { SearchResult, Products, Navbar, Cart, Customers, SignUp, Login, Profile, Stores, Categories, EditProduct, Checkout, ProfileStoreOwner, ProfileAdmin,AddProduct,ListProducts,Connector, AdminPage, Campaign, MyOrders} from './components';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import firebase from './components/Firebase/fire';
import {setCurrentUser} from './redux/User/user.action.js'
import {connect} from 'react-redux';
import {auth, createUserDocument} from './components/Firebase/fire.js';
import { Navigate } from 'react-router';
import WithAuth from './hoc/withAuth';
import WithAdminAuth from './hoc/withAdminAuth';
import DetailedProductPage from './components/Products/DetailedProductPage';
import StoreOwner from './components/StoreOwner/StoreOwner';
import {Button} from "@material-ui/core";
import Price_Ascending from './components/Products/Price_Ascending';
import Price_Descending from './components/Products/Price_Descending';
import Rate_Ascending from './components/Products/Rate_Ascending';
import Rate_Descending from './components/Products/Rate_Descending';
import Suggested from './components/Products/Suggested';

import {updateProductStatus} from './components/Firebase/fire';

const App = props => {        

    const [ products, setProducts ] = useState([]);
    const [ allStoreOwners, setStoreOwners ] = useState([]);
    const [ allproducts, setAllProducts ] = useState([]);
    const [ customers, setCustomers ] = useState([]);
    const [ loading, setLoading] = useState(false);
    const [ cartItems, setCartItems] = useState([]);
    const [ product, setProduct ] = useState([]);


    const refCustomers = firebase.firestore().collection("users");    
    const [searchResults, setSearchResults] = useState([]);
    const refProducts = firebase.firestore().collection("Products");

    function getProducts(){
        setLoading(true);
        refProducts.where("Product_Status","==","1").onSnapshot((querySnapshot) => {
            const items = [];
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            setProducts(items);
            setLoading(false);
        });
    }

    
    function getAllProducts(){
        setLoading(true);
        refProducts.onSnapshot((querySnapshot) => {
            const items = [];
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            setAllProducts(items);
            setLoading(false);
        });
    }

    function getStoreOwners(){
        setLoading(true);
        refCustomers.where("role","==","Company").onSnapshot((querySnapshot) => {
            const items = [];
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            setStoreOwners(items);
            setLoading(false);
        });
    }

    function getCustomers(){
        setLoading(true);
        refCustomers.onSnapshot((querySnapshot) => {
            const items = [];
            querySnapshot.forEach((doc) => {
                items.push(doc.data());
            });
            setCustomers(items);
            setLoading(false);
        });
    }

    const handleAddProduct = (product) => {
        const ProductExist = cartItems.find((item) => item.Product_Id === product.Product_Id);
        if(ProductExist){
            setCartItems(
                cartItems.map((item) =>
                item.Product_Id === product.Product_Id
                ? {...ProductExist, quantity: ProductExist.quantity + 1,PO_Status:"1"}
                : item
                )
            );
        } else {
            setCartItems([...cartItems, {...product, quantity:1,PO_Status:"1"}]);
        }
    }
    

    const handleRemoveProduct = (product) => {
        const ProductExist = cartItems.find((item) => item.Product_Id === product.Product_Id);
        if(ProductExist.quantity === 1){
            setCartItems(
                cartItems.filter((item) =>
                item.Product_Id !== product.Product_Id));
        } else {
            setCartItems(
            cartItems.map((item) =>
                item.Product_Id === product.Product_Id
                ? {...ProductExist, quantity: ProductExist.quantity - 1}
                : item
                )
            )            
        }
    }

    const handleClearCart = (product) =>{        
        setCartItems([]);
    }

   const{setCurrentUser, currentUser} = props;
   
    
    useEffect(() => {        
        getCustomers();
        getProducts();
        getAllProducts();
        getStoreOwners();


        const authListener = auth.onAuthStateChanged(async userAuth => {
            if (userAuth){
                const userRef = await createUserDocument(userAuth);
                userRef.onSnapshot(snapshot => {
                    setCurrentUser({
                        id: snapshot.id,
                        ...snapshot.data()
                    });
                })
            }
            setCurrentUser(userAuth);
        });

        return () => {
            authListener();
        };

    }, []); 
    
    if(loading) {
        return <h1>Loading...</h1>;
    }

    

    const searchCallbackHandler = (searchValue) => 
    { 
        if (searchValue && products) { 
            const tempProductArr = [...products]; 
            setSearchResults(tempProductArr.filter(p => p.Product_Name?.toUpperCase().includes(searchValue.toUpperCase())))             
            document.getElementById("search-button").click(); 
        } 
    }
    
    return (       
        <Router>            
            <div>                
                <Navbar cartItems={cartItems} searchCallback={searchCallbackHandler}/>
             
                <Button id="search-button" style={{display: "none"}} component={Link} to="/searchResult" color="inherit">                     Search                 </Button>

                <Routes>
                    <Route path='/' element={<Products products = {products} handleAddProduct={handleAddProduct} />} />
                    <Route path='/Suggested' element={<Suggested products = {products} handleAddProduct={handleAddProduct} />} />
                    <Route path='/Price_Ascending' element={<Price_Ascending products = {products} handleAddProduct={handleAddProduct} />} /> 
                    <Route path='/Price_Descending' element={<Price_Descending products = {products} handleAddProduct={handleAddProduct} />} /> 
                    <Route path='/Rate_Ascending' element={<Rate_Ascending products = {products} handleAddProduct={handleAddProduct} />} /> 
                    <Route path='/Rate_Descending' element={<Rate_Descending products = {products} handleAddProduct={handleAddProduct} />} />                                    
                    <Route path='/cart' element={
                    <Cart 
                        cartItems = {cartItems} 
                        handleAddProduct={handleAddProduct} 
                        handleRemoveProduct={handleRemoveProduct} 
                        handleClearCart={handleClearCart}
                    />} /> 
                    <Route path='/customers' element={<Customers customers = {customers}/>} />
                    <Route path="/register" element={ (<SignUp />)} />
                    <Route path="/login" element={(<Login />)} />
                    <Route path='/myaccount'element={ (<Profile products = {products}/>)} />   
                    <Route path='/myaccount_admin'element={ (<ProfileAdmin  allproducts={allproducts} allStoreOwners={allStoreOwners} />)} /> 
                    <Route path='/myaccount_storeowner'element={ (<ProfileStoreOwner />)} />               
                    <Route path= '/detailed_page/:productId' element = {<DetailedProductPage/>}  />               
                    <Route path='/stores' element={<Stores products = {products} handleAddProduct={handleAddProduct}  />} />       
                    <Route path='/categories' element={<Categories products = {products} handleAddProduct={handleAddProduct}  />} />
                    <Route path='/storeowner' element={<StoreOwner />} />   
                    <Route path='/Checkout' element={<Checkout cartItems={cartItems} />} />
                    <Route path='/EditProduct/:productId' element={<EditProduct />} />
                    <Route path='/AddProduct' element={<AddProduct />} />
                    <Route path='/ListProduct' element={<ListProducts products={products}/>} /> 
                    <Route path="/searchResult" element={<SearchResult searchResults={searchResults} handleAddProduct={handleAddProduct} />} />                     <Route path='/ListProduct' element={<ListProducts products={products}/>} />
                    <Route path='/Connector' element={ (<Connector />) } />  
                    <Route path='/AdminPage' element={ <AdminPage products={products}/> } />
                    <Route path='/Campaign' element={ <Campaign products={products} customers={customers}/> } /> 
                    <Route path='/myorders' element={ <MyOrders /> } />   
                </Routes>                            
            </div>
        </Router>
  );
};

export default App
