import React from 'react';
import { Button } from '@material-ui/core';
import useStyles from './styles';
import {useState, useEffect} from 'react';
import firebase from '../Firebase/fire';
import { useUserContext } from "../../context/userContext";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import PdfFile from '../PdfFile/PdfFile.js'


function MyOrders({ }) {
    const classes = useStyles();
    const { custEmail, displayName } = useUserContext();
    const db = firebase.firestore()
    const refPurchase = db.collection("purchase-order")
    const [order, setOrder] = useState([]);
    
    function fetchSoldProducts(){
        refPurchase.onSnapshot((querySnapshot) => {
          const items = [];
          querySnapshot.forEach((doc) => {
              items.push(doc.data());          
          });
          const list1 = [];

          Array.from(items).forEach(element => {
            const list2 =[];
            if(element.email === custEmail){
                const day = (element.PurchaseDate.toDate().toDateString()).substring(0, 3)
                const month = (element.PurchaseDate.toDate().toDateString()).substring(4, 7)
                const day2 = (element.PurchaseDate.toDate().toDateString()).substring(8, 10)
                const year = (element.PurchaseDate.toDate().toDateString()).substring(11, 15)
                const date = day2+"/"+month+"/"+year+","+day;                    
                const totalAmount = element.totalAmount;
                console.log(totalAmount);
                const buyerName = element.buyerName;
                const deneme2 = element.cartItems;
                const cartLength = element.cartItems.length;
                const addressToSend = element.addressToSend;
                const email = element.email;

                list2.push({
                    "date":date,  
                    "totalAmount":Number(totalAmount),
                    "cartLength":cartLength,
                    "buyerName":buyerName,
                    "addressToSend":addressToSend,
                    "email":email
                })
                
                Array.from(deneme2).forEach((key) => {
                list2.push({
                    "productName":key.Product_Name,
                    "productPrice":key.Product_Price,
                    "productQuantity":key.quantity,
                    })
                });
                list1.push(list2);                
            }  
            
          });
          setOrder(list1); 
    })
};
    let soldProducts = [];
    const generatePDF = (object) => {
        soldProducts = [];
        const date = object[0].date;
        const totalPrice = object[0].totalAmount;
        const buyerName = object[0].buyerName;
        const cartLength = object[0].cartLength;
        for (let i = 1; i < cartLength+1; i++) {
          let tmp;
          tmp = {
              "id" : i,
              "productName":object[i].productName,
              "price":object[i].productPrice,
              "soldDate":object[0].date,
              "soldCount":object[i].productQuantity
          };
          soldProducts.push(tmp);
      
        }
    }

    useEffect(() => {        
      fetchSoldProducts();
    }, []);

    return (
      <div>
      <div className={classes.toolbar} />
      <div className={classes.tableContent}>
        <TableContainer component={Paper}>
        <Table size="small" aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Date</TableCell>
              <TableCell align="center">Total Price</TableCell>
              <TableCell align="center">Amount of Products</TableCell>
              <TableCell align="left">Invoice</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {order.map((object) => (
              <TableRow
                key={object[0].date}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                {generatePDF(object)}
                <TableCell component="th" scope="row">
                  {object[0].date}                  
                </TableCell>
                <TableCell align="center">{object[0].totalAmount} TL</TableCell>
                <TableCell align="center">{object[0].cartLength} piece(s)</TableCell>
                <TableCell align="center">
                   
                    {/*<Button style={{textAlign:"center"}} vaiant="contained" color="success" onClick={() => generatePDF(object)}> PDF</Button>*/}
                    <PdfFile name={object[0].buyerName} address={object[0].addressToSend} soldProductsAttr={soldProducts} totalPrice={(object[0].totalAmount).toString()} to_email={object[0].email} to_name={object[0].buyerName}></PdfFile>
            
                    
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      </div>
      </div>
    );
  }
  
  export default MyOrders;
