import React from 'react'
import { Typography, Button, Card, CardActions, CardMedia, CardContent } from '@material-ui/core';

import useStyles from './styles';

const CartItem = ({ item, handleAddProduct, handleRemoveProduct, handleRemoveFromCart }) => {
    const classes = useStyles();
    return (
        <Card >
            <CardMedia image={item.Product_Picture} alt={item.Product_Name} className={classes.media}/>
            <CardContent className={classes.cardContent}>
                <Typography variant="h4">
                    {item.Product_Name}
                </Typography>
                <Typography variant="h5">
                    {item.Product_Price}
                </Typography>
            </CardContent>
            <CardActions className={classes.cardActions}>
                <div className={classes.buttons}>
                    <Button type="button" size="small" onClick={() => handleRemoveProduct(item)}>-</Button>
                    <Typography>{item.quantity}</Typography>
                    <Button type="button" size="small" onClick={() => handleAddProduct(item)}>+</Button>
                </div>
                {/*<Button variant="contained" type="button" color="secondary" onClick={() => handleRemoveFromCart(item.Product_Id)}>Remove</Button>*/}
            </CardActions>
        </Card>
  )
}

export default CartItem
