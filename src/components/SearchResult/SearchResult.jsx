import React, {useEffect, useState} from 'react';
import {Button, Container, Grid} from '@material-ui/core';
import useStyles from './styles';
import Product from '../Products/Product/Product';

import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import {Link} from "react-router-dom";

function SearchResult({searchResults , handleAddProduct}) {
    const classes = useStyles();
    const [products, setProducts] = useState(searchResults);
    const [productList, setProductList] = useState(searchResults);
    const [sortPriceDesc, setSortPriceDesc] = useState(false);
    const [sortRatingDesc, setSortRatingDesc] = useState(false);


    useEffect(() => {
        if (products) {
            setProductList(products);
        }
    }, [products])


    const sortByPriceHandler = () => {

        const tempArray = [...productList];

        setProductList(tempArray?.sort((p1, p2) => {
            return sortPriceDesc ? (parseInt(p2.Product_Price) - parseInt(p1.Product_Price)) : (parseInt(p1.Product_Price) - parseInt(p2.Product_Price));            
        }))

        setSortPriceDesc(!sortPriceDesc);
    }

    const sortByRatingHandler = () => {
        const tempArray = [...productList];
        console.log(productList);
        setProductList(tempArray?.sort((p1, p2) => {
            return sortRatingDesc ? (parseInt(p2.Product_Available) - parseInt(p1.Product_Available)) : (parseInt(p1.Product_Available) - parseInt(p2.Product_Available));            
        }))
        setSortRatingDesc(!sortRatingDesc);
    }

    return (
        <Container>

            <div className={classes.toolbar}>

            </div>
            <main className={classes.root}>
                <div className={classes.toolbar}>
                    <Button component={Link} to="/" color="inherit">
                        Back
                    </Button>
                </div>
                {products && products?.length > 0 && (<> <Button variant="text" style={{float: "right", marginRight: "20%"}}
                                     endIcon={sortPriceDesc ? <ArrowDownwardIcon/> : <ArrowUpwardIcon/>}
                                     onClick={sortByPriceHandler}>
                    Price
                </Button> 

                <Button variant="text" style={{float: "right", marginRight: "20%"}}
                                     endIcon={sortRatingDesc ? <ArrowDownwardIcon/> : <ArrowUpwardIcon/>}
                                     onClick={sortByRatingHandler}>
                    Stock
                </Button>   </>)}
                
                <Grid container justify="center" spacing={4}>
                    {productList && productList?.length > 0 ? (productList?.map((product) => (
                        <>
                            <Grid item key={product.Product_Seller} xs={12} sm={6} md={3} lg={2}>
                                <Product product={product} handleAddProduct={handleAddProduct}/>
                            </Grid>
                        </>
                    ))) : (<div>There is no item</div>)}
                </Grid>

            </main>
        </Container>
    );
}

export default SearchResult;
