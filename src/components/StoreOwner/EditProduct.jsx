import {React, useState, useEffect} from 'react';
import { Grid} from '@material-ui/core';
import useStyles from './styles';
import { Card, CardMedia, CardContent, Typography} from '@material-ui/core';
import { useParams } from 'react-router-dom';
import firebase from '../Firebase/fire';

const EditProduct = ({ }) => {
    const classes = useStyles();
    const params = useParams();
    var productId = Number(params.productId);

    const [ product, setProduct ] = useState([]);
    const SingleProduct = firebase.firestore().collection("Products").where("Product_Id","==",productId)

    const [productDiscount , SetDiscount] = useState("");
    const [productAvaliable , SetAvaliable] = useState("");
    const [productCategory , SetCategory] = useState("");
    const [product_Id , SetProductId] = useState("");
    const [productName  , SetName] = useState("");
    const [productPrice , SetPrice] = useState("");
    const [productSeller , SetSeller] = useState("");
    const [productPicture , SetPicture] = useState("");
    const db = firebase.firestore()
    
   

    const sub = (e) => {
        e.preventDefault();

        // Edit data
        db.collection("Products").doc(product_Id[0]).update({
            Product_Discount: productDiscount,
            Product_Available: productAvaliable,
            Product_Category: productCategory,
            Product_Id: productId,
            Product_Name: productName,
            Product_Price: productPrice,
            Product_Seller: productSeller,
            //Product_Picture: productPicture,
        })
        .then((docRef) => {
            alert("Data Successfully Submitted");
        })
        .catch((error) => {
            console.error("Error adding document: ", error);
        });
    }
  
    function getProducts(){
        setLoading(true);
        SingleProduct.onSnapshot((querySnapshot) => {
            const items = [];
            const docId = [];
            querySnapshot.forEach((doc) => {
                docId.push(doc.id);
                items.push(doc.data());
            });
            SetProductId(docId);
            setProduct(items);
            setLoading(false);
        });
    }

    useEffect(() => {        
        getProducts();
        
    }, []); 


    return (
        <Grid container justify="center" style={{marginTop:"150px" }} spacing={4}>
            <Grid item key={product.Product_Id} style={{marginRight:"150px" }} xs={12} sm={6} md={3} lg={2}>
                {product.map((product) => (                                                                                            
                    <Card className={classes.root}>
                        <CardMedia className={classes.media} image={product.Product_Picture} title={product.Product_Name} />
                        <CardContent>
                            <div className={classes.cardContent}>
                                <Typography variant="h5" component="h2" gutterBottom>      
                                    {product.Product_Name}
                                </Typography>
                                <Typography variant="h5" component="h2" gutterBottom style={{ fontWeight: 600 }}>
                                    {product.Product_Price} TL
                                </Typography>
                            </div>
                            <Typography variant="body2" color="textSecondary" component="p" gutterBottom>
                                    Stock: {product.Product_Available}
                            </Typography>
                        </CardContent>            
                    </Card>
                ))}
        </Grid>
        <Grid>
            {product.map((product) => ( 
                <form
                onSubmit={(event) => {sub(event)}}>
                    <h2>Edit Product</h2>
                    <Typography>Product Name: {product.Product_Name} </Typography>
                    <input type="text" required="required" placeholder="Product Name"
                    onChange={(e)=>{SetName(e.target.value)}} />
                    <br/><br/>
                    <Typography>Product Category: {product.Product_Category} </Typography>
                    <input type="text" required="required" placeholder="Product Category"
                    onChange={(e)=>{SetCategory(e.target.value)}}/>
                    <br/><br/>
                    <Typography>Product Seller: {product.Product_Seller} </Typography>  
                    <input type="text" required="required" placeholder="Product Seller"
                    onChange={(e)=>{SetSeller(e.target.value)}}/>
                    <br/><br/>
                    <Typography>Product Price: {product.Product_Price} TL </Typography>
                    <input type="text" required="required" placeholder="Product Price"
                    onChange={(e)=>{SetPrice(e.target.value)}}/>
                    <br/><br/>
                    <Typography>Product Stock: {product.Product_Available}</Typography>
                    <input type="text" required="required" placeholder="Product Stock"
                    onChange={(e)=>{SetAvaliable(e.target.value)}}/>
                    <br/><br/>                
                    <Typography>Product Discount: {product.Product_Discount} %</Typography>
                    <input type="text" required="required" placeholder="Product Discount"
                    onChange={(e)=>{SetDiscount(e.target.value)}}/>
                    <br/><br/>
                    <Typography>Product Picture URL (Optional):</Typography>
                    <input type="text" placeholder="Product Picture"
                     onChange={(e)=>{SetPicture(e.target.value)}}/>
                    <br/><br/>
                    <button variant="contained" color="success" size="large" type="submit">Submit</button>
                </form>
                ))}
        </Grid>
    </Grid>
    );
}

export default EditProduct;