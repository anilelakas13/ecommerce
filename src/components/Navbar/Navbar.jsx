import React, { useState, useEffect } from "react";
import {
  AppBar,
  Badge,
  Button,
  IconButton,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from "@material-ui/core";
import { AccountCircle, ShoppingCart } from "@material-ui/icons";
import MenuIcon from "@material-ui/icons/Menu";
import { Link, useLocation } from "react-router-dom";
import logo from "../../assets/commerce.png";
import useStyles from "./styles";
import fire from "../Firebase/fire";
import SearchIcon from "@material-ui/icons/Search";
import { alpha, styled } from "@material-ui/core/styles";
import InputBase from "@material-ui/core/InputBase";
import { useUserContext } from "../../context/userContext";

const Navbar = ({ cartItems, searchCallback }) => {
  const classes = useStyles();
  const { user, role } = useUserContext();
  const [userName, setUserName] = useState("");
  const location = useLocation();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const [anchorEl2, setAnchorEl2] = React.useState(null);
  const open2 = Boolean(anchorEl2);
  const [searchValue, setSearchValue] = useState("");
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleClick2 = (event) => {
    setAnchorEl2(event.currentTarget);
  };
  const handleClose2 = () => {
    setAnchorEl2(null);
  };

  const handleLogout = () => {
    fire.auth().signOut();
  };

  const Search = styled("div")(({ theme }) => ({
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: alpha(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: alpha(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  }));

  const SearchIconWrapper = styled("div")(({ theme }) => ({
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  }));

  const StyledInputBase = styled(InputBase)(({ theme }) => ({
    color: "inherit",
    "& .MuiInputBase-input": {
      padding: theme.spacing(1, 75, 1, 5),
      // vertical padding + font size from searchIcon
      paddingLeft: `calc(1em + ${theme.spacing(4)})`,
      transition: theme.transitions.create("width"),
      width: "100%",
      [theme.breakpoints.up("md")]: {
        width: "50ch",
      },
    },
  }));

  const searchHandler = (e) => {
    if (e.key === "Enter") {
      setSearchValue(e.target.value);
      searchCallback(e.target.value);
    }
  };

  useEffect(() => {
    if (user != null && user.displayName != null) {
      setUserName(user.displayName);
    }
  });

  return (
    <>
      <AppBar
        position="fixed"
        className={classes.appBar}
        style={{ background: "#ff6900" }}
      >
        <Toolbar>
          {/*<Typography variant="h5"
            className={classes.title}
            color="inherit">
            {userName}  
          </Typography>*/}
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="menu"
            onClick={handleClick}
            sx={{ mr: 2 }}
          >
            <MenuIcon />
          </IconButton>
          <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            onMouseOver={open}
            open={open}
            onClose={handleClose}
            MenuListProps={{
              "aria-labelledby": "basic-button",
            }}
          >
            <MenuItem onClick={handleClose} component={Link} to="/categories">
              Categories
            </MenuItem>

            <MenuItem onClick={handleClose} component={Link} to="/stores">
              Stores
            </MenuItem>
          </Menu>

          <Typography
            component={Link}
            to="/"
            variant="h5"
            className={classes.title}
            color="inherit"
          >
            <img
              src={logo}
              alt="Commerce.js"
              height="45px"
              className={classes.image}
            />
            Tükkan CS576
          </Typography>
          {location.pathname !== "/searchResult" && (
            <Search style={{ width: "40%" }} onKeyPress={searchHandler}>
              <SearchIconWrapper>
                <SearchIcon />
              </SearchIconWrapper>
              <StyledInputBase
                placeholder="Search…"
                defaultValue={searchValue}
                inputProps={{ "aria-label": "search" }}
              />
            </Search>
          )}
          <div className={classes.grow} />

          {!user && location.pathname !== "/login" && (
            <Button component={Link} to="/login" color="inherit">
              Login
            </Button>
          )}
          {!user && location.pathname !== "/register" && (
            <Button component={Link} to="/register" color="inherit">
              Register
            </Button>
          )}

          {user && (
            <Button
              component={Link}
              to="/"
              onClick={handleLogout}
              color="inherit"
            >
              Logout
            </Button>
          )}

          {user ? (
            <div>
              <IconButton
                size="small"
                edge="end"
                aria-label="account of current user"
                aria-controls="basic-menu"
                aria-haspopup="true"
                onClick={handleClick2}
                color="inherit"
              >
                <AccountCircle />
              </IconButton>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl2}
                open={open2}
                onClose={handleClose2}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                {role === "Personal" && (
                  <MenuItem
                    onClick={handleClose2}
                    component={Link}
                    to="/myaccount"
                  >
                    My Account
                  </MenuItem>
                )}

                {role === "Personal" && (
                  <MenuItem
                    onClick={handleClose2}
                    component={Link}
                    to="/myorders"
                  >
                    My Orders
                  </MenuItem>
                )}

                {role === "Company" && (
                  <MenuItem
                    onClick={handleClose2}
                    component={Link}
                    to="/myaccount_storeowner"
                  >
                    My Account
                  </MenuItem>
                )}

                {role === "Company" && (
                  <MenuItem
                    onClick={handleClose2}
                    component={Link}
                    to="/storeowner"
                  >
                    Store Owner
                  </MenuItem>
                )}

                {role === "Admin" && (
                  <MenuItem
                    onClick={handleClose2}
                    component={Link}
                    to="/myaccount_admin"
                  >
                    My Account
                  </MenuItem>
                )}

                {role === "Admin" && (
                  <MenuItem
                    onClick={handleClose2}
                    component={Link}
                    to="/AdminPage"
                  >
                    Admin
                  </MenuItem>
                )}

                {user && (
                  <Button onClick={handleLogout} color="inherit">
                    Logout
                  </Button>
                )}
              </Menu>
            </div>
          ) : (
            <div></div>
          )}
          {user ? (
            <div className={classes.button}>
              <IconButton
                component={Link}
                to="/cart"
                aria-label="Show cart items"
                color="inherit"
              >
                <Badge
                  badgeContent={cartItems.lenght === 0 ? "" : cartItems.length}
                  color="secondary"
                >
                  <ShoppingCart />
                </Badge>
              </IconButton>
            </div>
          ) : (
            <div></div>
          )}
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Navbar;
