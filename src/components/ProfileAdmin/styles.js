import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  root: {
    MaxWidth: '100%',    
  },
  media: {
    height: 240,
    width:240,
    paddingTop: '56.25%', // 16:9
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  cardContent: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));