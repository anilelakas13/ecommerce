import React, { useState, useRef } from "react";
import { useUserContext } from "../../context/userContext";
import "./styles.css";
import { auth, createUserDocument } from "../Firebase/fire";
import { Link } from "react-router-dom";
import loginbackground from "../../assets/loginpage.jpg";
import emailjs from "emailjs-com";

const SignUp = () => {
  const emailRef = useRef();
  const nameRef = useRef();
  const psdRef = useRef();
  const phoneNumberRef = useRef();
  const confirm_psdRef = useRef();

  const [emailError, setEmailError] = useState("");
  const [passwodError, setPasswordError] = useState("");

  const { registerUser } = useUserContext();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [role, setRole] = useState();

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    const email = emailRef.current.value;
    const name = nameRef.current.value;
    const password = psdRef.current.value;
    const phone = phoneNumberRef.current.value;
    const confirmPassword = confirm_psdRef.current.value;

    if (password !== confirmPassword) {
      const err = ["Error -  Password do not match!"];
      setEmailError(err);
      return;
    }

    if (email && password && name && role && phone) {
      registerUser(email, password, name, phone, role);
    } else {
      setEmailError("Error - All inputs are required. You can not register");
    }

    emailjs.init("9qipjvqAhN_zEkeDW");
    emailjs.send("service_1pripqs", "template_2rdi6gc", {
      to_email: email,
      to_name: name,
    });
    const { user } = await auth

      .createUserWithEmailAndPassword(email, password)
      .catch((err) => {
        // eslint-disable-next-line default-case
        switch (err.code) {
          case "auth/email-already-in-use":
          case "auth/invalid-email":
            setEmailError(err.message);
            break;
          case "auth/weak-password":
            setPasswordError(err.message);
            break;
        }
      });
    resetForm();
    console.log(user);

    if (email && password && name && role && phone) {
      await createUserDocument(user, { name, role, phone });
    } else {
      setEmailError("Error - All inputs are required.");
    }
  };

  const myStyle = {
    backgroundImage: `url(${loginbackground})`,
    height: "100vh",
    backgroundSize: "cover",
    opacity: 0.8,
    backgroundRepeat: "no-repeat",
  };

  const resetForm = () => {
    setEmail("");
    setPassword("");
  };

  return (
    <div style={myStyle}>
      <form className="signup-login">
        <h1>REGISTER</h1>
        <div class="radio-buttons">
          <label class="radio-inline">
            <input
              type="radio"
              required
              id="Personal"
              name="fav_language"
              value="Personal"
              onChange={(e) => setRole(e.target.value)}
            />
            Personal
          </label>
          <label class="radio-inline">
            <input
              type="radio"
              required
              id="Company"
              name="fav_language"
              value="Company"
              onChange={(e) => setRole(e.target.value)}
            />
            Company
          </label>
        </div>

        <input
          type="displayName"
          autoFocus
          required
          placeholder="Full name"
          ref={nameRef}
        />
        <input
          type="phoneNumber"
          autoFocus
          required
          placeholder="Phone number"
          ref={phoneNumberRef}
        />
        <input
          type="email"
          autoFocus
          required
          placeholder="Email"
          ref={emailRef}
        />
        <input
          type="password"
          autoFocus
          required
          placeholder="Password"
          ref={psdRef}
        />
        <input
          type="password"
          autoFocus
          required
          placeholder="Confirm Password"
          ref={confirm_psdRef}
        />
        <button
          type="submit"
          color="primary"
          className="px-4"
          onClick={handleFormSubmit}
        >
          Register
        </button>
        <p className="errorMsg">{passwodError}</p>
        <Link to="/login">Do you have an account?</Link>
        <p className="errorMsg">{emailError}</p>
      </form>
    </div>
  );
};

export default SignUp;
