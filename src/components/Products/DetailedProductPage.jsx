import { React, useState, useEffect, useCallback } from "react";
import { Grid, Divider, Typography } from "@material-ui/core";
import { useParams } from "react-router-dom";
import useStyles from "./styles";
import firebase from "../Firebase/fire";
import "./detailedstyles.css";
import Rating from "@mui/material/Rating";
import TextField from "@mui/material/TextField";
import { Button } from "semantic-ui-react";
import SendIcon from "@mui/icons-material/Send";
import { useUserContext } from "../../context/userContext";

const DetailedProductPage = () => {
  const classes = useStyles();
  const params = useParams();
  const productId = params.productId;
  const [products, setProducts] = useState([]);
  const [documentId, setDocumentId] = useState();
  const [comments, setComments] = useState([]);
  const [ratingValue, setValue] = useState(5);
  const { user, forgotPassword, role, displayName, phone, custEmail } =
    useUserContext();

  const DetailProduct = firebase
    .firestore()
    .collection("Products")
    .where("Product_Id", "==", parseInt(productId));

  const commentList = firebase
    .firestore()
    .collection("Products/" + documentId + "/Comments&Ratings")
    .where("Comment_Status", "==", 1);

  const [newComment, sendComment] = useState("");

  // const productComments = firebase
  //   .firestore()
  //   .collection("Products")
  //   .where("Product_Id", "==", productId)
  //   .collection("Comments&Ratings");

  // const DUMMY = {
  //   Product_Available: 28,
  //   Product_Category: "Tulip",
  //   Product_Id: "4",
  //   Product_Name: "Yellow Tulip Bucket",
  //   Product_Picture:
  //     "https://media.istockphoto.com/photos/yellow-tulip-flowers-in-glass-vase-picture-id120561388?k=20&m=120561388&s=170667a&w=0&h=OHUeTlTN6sYL2ZHQFDHL0ZfWIiQmOdrsbK1z6G2H3jY=",
  //   Product_Price: 11,
  //   Product_Rate: 0,
  //   Product_Seller: "Golden Flower",
  //   Product_Comment:
  //     "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus sed libero efficitur, scelerisque est ut, tristique magna. Donec enim ipsum, maximus vitae convallis id, vehicula ac nibh. Vestibulum pharetra vitae felis sed molestie. Nunc ut sem tempus, egestas nisl non, ullamcorper nunc.",
  //   Product_UserComment: [
  //     "Phasellus sed libero efficitur, scelerisque est ut.",
  //     "Donec enim ipsum, maximus vitae convallis id",
  //   ],
  // };

  // const DUMMYCOMMENTS = {
  //   Comment: ["Smelly Flowers", "Nice Flowers"],
  //   Comment_by_User: ["Kaan", "Farukcan Şoray"],
  //   Product_rating: [2, 4],
  // };

  const getProducts = useCallback(() => {
    DetailProduct.onSnapshot((querySnapshot) => {
      const items = [];
      querySnapshot.forEach((doc) => {
        items.push(doc.data());
        setDocumentId(doc.id);
        console.log(doc.data());
      });
      setProducts(items);
    });

    commentList.onSnapshot((querySnapshot) => {
      const items = [];
      querySnapshot.forEach((doc) => {
        items.push(doc.data());
      });
      setComments(items);
    });
  }, [DetailProduct, commentList]);

  useEffect(() => {
    if (!products[0] && !comments[0]) getProducts();
  }, [getProducts, products, comments]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log(newComment);
    console.log("aa", products[0]);

    const collectionRef = firebase.firestore().collection("Products");

    const query = await collectionRef
      .where("Product_Id", "==", parseInt(productId))
      .get();

    query.forEach((doc) => {
      console.log(doc.id, "=>", doc.data());
      firebase
        .firestore()
        .collection("Products")
        .doc(doc.id)
        .collection("Comments&Ratings")
        .add({
          Comment: newComment,
          Product_Rating: ratingValue,
          Comment_by_User: displayName,
          Comment_by_email: custEmail,
          Product_Rating: ratingValue,
          Comment_Status: 0,
        });
    });
  };
  /*products.map((item, product) => {
    <tr key={product}>
      <td>{item.Product_Name}</td>
      <td>{item.Product_Picture}</td>
      <td>{item.Product_Price}</td>
    </tr>;
  });*/

  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      {products[0] !== undefined && (
        <div className="master-container">
          <div className="product-container">
            <h1>{products[0].Product_Name ?? ""}</h1>
            <div className="rightleft-container">
              <div className={classes.picture} style={{}}>
                <img
                  src={products[0].Product_Picture}
                  alt="no_image"
                  width="300px"
                  height="300px"
                />
              </div>

              <div className="right-container" style={{}}>
                <h3 style={{}}>Seller: {products[0].Product_Seller} </h3>
                <h3 className="rating">
                  {/* <Icon>
                    <StarIcon />
                  </Icon>{" "} */}
                  <Rating
                    name="read-only"
                    value={products[0].Product_Rating}
                    readOnly
                  />{" "}
                  {products[0].Product_Rate ?? ""}/5
                </h3>
                <p className="description">
                  {" "}
                  {products[0].Product_Comment ?? ""}{" "}
                </p>
              </div>
            </div>
          </div>
          <div className="bottom-container" style={{}}>
            <h3>Add a comment</h3>
            <form onSubmit={handleSubmit}>
              <TextField
                style={{ width: 500 }}
                multiline
                id="outlined-basic"
                variant="outlined"
                value={newComment}
                onChange={(e) => sendComment(e.target.value)}
              />
              <Divider />
              <Typography component="legend"></Typography>
              <Rating
                name="simple-controlled"
                value={ratingValue}
                onChange={(event, newValue) => {
                  setValue(newValue);
                }}
              />
              <Button variant="contained" type="submit" endIcon={<SendIcon />}>
                Send
              </Button>
            </form>

            <h2>Comments</h2>

            {comments.map((comment, index) => {
              return (
                <div key={"comment" + index}>
                  <Grid container direction="column" spacing={2}>
                    <Grid item>
                      {comment.Comment_by_User}{" "}
                      <Rating
                        name="read-only"
                        value={comment.Product_Rating}
                        readOnly
                      />
                      {comment.Product_Rating}/5
                    </Grid>
                    <Grid item>{comment.Comment}</Grid>
                  </Grid>
                  <div>
                    <Divider />
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      )}

      {/*{products.map((product) => (
          <img src={product.Product_Picture} width="" height="65"></img>
        ))}
        {products.map((item, product) => {
           return <tr key={product}>
            <td>{item.Product_Name}</td>
            <td>{item.Product_Picture}</td>
            <td>{item.Product_Price}</td>
          </tr>;
        })}

    <p>{products.Product_Picture}</p> }
         </div>
      <Grid container justify="center" spacing={4}>
        {/*Detailed Product Page {productId} {DetailProduct.Product_Name}

        {products.map((product) => (
          <h2>{product.Product_Name}</h2>
        ))}
      </Grid>*/}
    </main>
  );
};

export default DetailedProductPage;
