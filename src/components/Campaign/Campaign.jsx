import {React, useState, useEffect} from 'react';
import { Grid, Container  } from '@material-ui/core';
import useStyles from './styles';
import ProductStoreOwner from '../Products/Product/ProductStoreOwner';
import { useUserContext } from "../../context/userContext";
import { Card, CardMedia, CardContent, Typography} from '@material-ui/core';
import firebase from '../Firebase/fire';
import emailjs from 'emailjs-com';


function Campaign({ products, customers }) {
  const classes = useStyles();
  const [productDiscount , SetDiscount] = useState("");
  const { user, role, displayName, userStatus, forgotPassword, logoutUser } = useUserContext();
  const db = firebase.firestore()
  const store_products  = db.collection("Products").where("Product_Seller","==",displayName);

  const sub = (e) => {
    e.preventDefault();

    store_products.get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            doc.ref.update({
                Product_Price: doc.data().Product_Price - ((doc.data().Product_Price * productDiscount) / 100)
            });
        });
    });
    
    for (let i = 0; i < customers.length; i++) {
      if(customers[i].role !== null && customers[i].role === "Personal"){
        emailjs.init("9qipjvqAhN_zEkeDW");
        emailjs.send("service_1pripqs","template_t0xw1lr",{
          to_email: customers[i].email,
          from_name: displayName,
          store_owner: displayName,
          discount_ratio: productDiscount,
      });
    }
  } 
}  

  return (

    <Container>
        <div className={classes.toolbar}/>
        <h2>Welcome {displayName}!</h2>
        <main className={classes.content}>
            <div className={classes.toolbar} />
            <Grid container justify="center" spacing={4} >
                {products.map((product) => (
                  <>
                  {product.Product_Seller === displayName &&(
                    <Grid item key={product.Product_Id} xs={12} sm={6} md={3} lg={2}>
                      <ProductStoreOwner product={product} />
                    </Grid>
                    )}
                  </>
                ))}
            </Grid>
            <Grid>
                <form
                onSubmit={(event) => {sub(event)}}>
                    <h2>Campaign</h2>                    
                    <Typography> Discount on All Products </Typography>
                    <input type="text" required="required" placeholder=" Discount Rate %"
                    onChange={(e)=>{SetDiscount(e.target.value)}}/>
                    <br/><br/>                    
                    <button variant="contained" color="success" size="large" type="submit">Submit</button>
                </form>                
        </Grid>
        </main>
    </Container>
  );
}

export default Campaign;