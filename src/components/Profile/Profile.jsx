import React, { useState, useRef } from "react";
import "./styles.css";
import firebase from "../Firebase/fire";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faMessage,
  faDolly,
  faPen,
  faKey,
  faDollyFlatbed,
  faArrowCircleRight,
} from "@fortawesome/free-solid-svg-icons";
import { Card, CardMedia, CardContent, Typography} from '@material-ui/core';
import { useUserContext } from "../../context/userContext";

const Profile = () => {
  const { user, forgotPassword, role ,displayName, phone,custEmail} = useUserContext();

  const [selectedItem, setSelectedItem] = useState(0);
  const [displayName_, setDisplayName] = useState(displayName);

  const [phonenumber, setPhonenumber] = useState("");
  const [curUser, setCurUser] = useState([]);
  const [curUsername, setCurUsername] = useState("");
  const [ personalID, setPersonalID] = useState("");
  const [currentPhoneNumber, setCurPhnNum] = useState(phone);

  const[ clicked, setClicked] = useState(0);
  const [message, setmessage] = useState("");

  var storeArr = [];

  const emailRef = useRef();

  const handleLogout = () => {
    firebase.auth().signOut();
  };


  const changeDiplayname = async (name) => {

    firebase
    .firestore()
    .collection("users")
    .doc(user.uid)
    .update({ displayName: name });
   setDisplayName(name);
  };

  const changePhoneNumber = async (name) => {

    firebase
    .firestore()
    .collection("users")
    .doc(user.uid)
    .update({ phone: name });
    setCurPhnNum(name);
  };

  const getUserInfo = async () => {
    const verfctnRef = firebase.firestore().collection(`users`);
    const snapshot = await verfctnRef.where("email", "==", user.email).get();

    if (snapshot.empty) {
      console.log("No matching documents.");
      return;
    }

    snapshot.forEach(function (doc) {
      const data = doc.data();

      storeArr.push(data);

      setDisplayName([data.displayName]);
    });

  };

  const verification = async () => {

    const verfctnRef = firebase.firestore().collection(`users`);
    const snapshot = await verfctnRef.where("email", "==", "pelin.oksuz@ozu.edu.tr").get();

    if (snapshot.empty) {
      console.log("No matching documents.");
      return;
    }

    snapshot.forEach(function (doc) {
      const data = doc.data();
      storeArr.push(data);

      setPersonalID(snapshot.docs[snapshot.docs.length-1].id);

    });
  };


  const forgotPasswordHandler = (e) => {
    forgotPassword(custEmail);  
    setmessage("Please, check your email box to change your password!");
  };

  return (
    <div className="wrapper">
<div class="container">
      <div className="leftbox">
        <nav>
          <a
            onClick={() => {
              setSelectedItem(0);
              verification();

            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faUser} ></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(5);

            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faPen}
                onClick={() => {
                  getUserInfo();
                }}></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(4);

            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faKey}></FontAwesomeIcon>
            </span>
          </a>
        
          
        </nav>
      </div>
      <div className="rightbox">
        {selectedItem === 0 ? (
          <div>
            <h1>{role} Info</h1>
            <h2>E Mail Address:</h2>
            <h3>{user.email}</h3>
            <h2>Username:</h2>
            <h3>{displayName_}</h3>
            <h2>role:</h2>
            <h3>{role}</h3>
            <h2>Phone</h2>
            <h3>{currentPhoneNumber}</h3>
          </div>
        ) : selectedItem === 4 ? (
          <div>
            <h1> CHANGE PASSWORD </h1>
            <h2>An e-mail will be sent to change your password. </h2>

            <div>
            <p onClick={forgotPasswordHandler}>
            <button id="button">Send</button></p>
            <p> {message}</p>
            </div>
          </div>
        ) : selectedItem === 2 ? (
          <div>
            <h1>Comments</h1>
            <h2>No Comment...</h2>
            <p1> Comment on your previous orders </p1>

            <a
              onClick={() => {
                setSelectedItem(3);

              }}
              class="tab"
            >
              <span className="dolly">
                <FontAwesomeIcon icon={faDolly}></FontAwesomeIcon>
              </span>
            </a>
          </div>
        )  : selectedItem === 5 ? (
          <div>
            <div>
            <form>
              
              <h2>Edit UserName</h2>
              <Typography>User Name: {displayName_}</Typography>
              <input type="text" required="required" placeholder="User Name"
              onChange={(e)=>{changeDiplayname(e.target.value)}} />
              <br/><br/>
              <Typography>Phone Number: {currentPhoneNumber}</Typography>
              <input type="text" required="required" placeholder="Your Phone Number"
              onChange={(e)=>{changePhoneNumber(e.target.value)}} />
              <br/><br/>

            </form>
          </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </div>
    </div>
    
  );
};
export default Profile;
