import React, {useEffect, useState} from 'react';
import logo from "../../assets/commerce.png";
import {Button} from '@material-ui/core';
import jspdf from 'jspdf';
import emailjs from 'emailjs-com';
import useStyles from './styles';

const PdfFile = (props)=> {

    const classes = useStyles();
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    var yyyy = today.getFullYear();
    today = dd + '.' + mm + '.' + yyyy;

    var productsCount = props.soldProductsAttr.length;

    const generatePdf = () => {
        var doc = jspdf('portrait','px','a4','false');
        doc.addImage(logo,'PNG',65,20,50,50);
        doc.setFont('Helvetica','bold');
        doc.setFontSize(8);
        doc.text(400,20,today);
        doc.setFontSize(12);
        doc.text(10,100,'Name:');
        doc.text(10,120,'Address:');

        doc.setFontSize(13);
        //doc.text(125,250,'Products');

        doc.text(20,250,'Product Name');
        doc.text(200,250,'Sold Date');
        doc.text(275,250,'Sold Count');
        doc.text(350,250,'Product Price');
        doc.line(10,255,425,255);

        

        doc.setFontSize(12);        
        doc.setFont('Helvetica','normal');
        doc.text(70,100,props.name);
        doc.text(70,120,props.address);
        
        var yAxis = 275;
        var xAxis = 70; 
        for(let i = 0; i < productsCount; i++){ 
            var productName = props.soldProductsAttr[i].productName;
            var productPrice = props.soldProductsAttr[i].price.toString();
            var productSoldDate = props.soldProductsAttr[i].soldDate;
            var productsoldCount = props.soldProductsAttr[i].soldCount.toString();            
            

            doc.text(20,yAxis,productName);
            doc.text(200,yAxis,productSoldDate);
            doc.text(285,yAxis,productsoldCount);
            doc.text(350,yAxis,productPrice);

            yAxis = yAxis + 15;
            //console.log(productName,productPrice,productSoldDate,productsoldCount);
        }

        doc.setFont('Helvetica','bold');
        doc.setFontSize(13);
        doc.text(300,yAxis+20,'Total Price:');
        doc.text(360,yAxis+20,props.totalPrice);

        var binary = doc.output();
        var base64= btoa(binary);
        
        emailjs.init("9qipjvqAhN_zEkeDW");
        emailjs.send("service_1pripqs","template_yji1k7i",{
            to_email: props.to_email,
            to_name: props.to_name,
            content: base64,
            });
        doc.save('invoice.pdf');
    }


    return (
    <div> 
        <div className={classes.pdfButton}>
            <Button variant="contained" disableElevation onClick={generatePdf}>PDF</Button>
        </div>
    </div>);
}

export default PdfFile;