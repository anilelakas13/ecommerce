import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing(3),
  },
  root: {
    MaxWidth: '100%',    
  },
  media: {
    height: 400,
    paddingTop: '56.25%', // 16:9
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'flex-end',
  },
  cardContent: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  tableContent: {
    textAlign: 'center',
    marginTop: 35,
    marginLeft: 400,
    width: 900,
  },
  pdfButton: {
    backgroundColor: 'red',
    width: '80px',
    height: '33px'
  },
}));