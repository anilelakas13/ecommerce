import {
  Card,
  CardMedia,
  CardContent,
  CardActions,
  Typography,
  IconButton,
} from "@material-ui/core";
import ChangeCircleIcon from "@mui/icons-material/ChangeCircle";
import firebase from "../Firebase/fire";
import React, { useState } from "react";

import useStyles from "./styles";

const Product_Status = ({ product }) => {
  const classes = useStyles();
  const [tabSelection, setTabSelection] = useState([]);

  const changeProductStatus = async (product) => {
    const refProducts = firebase
      .firestore()
      .collection("Products")
      .where("Product_Id", "==", product.Product_Id);

    if (product.Product_Status === "1") {
      refProducts.get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.update({
            Product_Status: "0",
          });
        });
      });
    } else if (product.Product_Status === "0") {
      refProducts.get().then(function (querySnapshot) {
        querySnapshot.forEach(function (doc) {
          doc.ref.update({
            Product_Status: "1",
          });
        });
      });
    }
    setTabSelection(3);
    console.log(product);
  };

  return (
    <Card className={classes.root}>
      <CardMedia
        className={classes.media}
        image={product.Product_Picture}
        title={Product_Status.Product_Name}
      />
      <CardContent>
        <div className={classes.cardContent}>
          <Typography variant="h5" component="h2" gutterBottom>
            {product.Product_Name}
          </Typography>
          <Typography
            variant="h5"
            component="h2"
            gutterBottom
            style={{ fontWeight: 600 }}
          >
            {product.Product_Price} TL
          </Typography>
        </div>
        <Typography
          variant="body2"
          color="textSecondary"
          component="p"
          gutterBottom
        >
          Status:{" "}
          {product.Product_Status == "1" ? (
            <div>Unsuspend</div>
          ) : (
            <div>Suspend</div>
          )}
        </Typography>
      </CardContent>
      <CardActions disableSpacing className={classes.cardActions}>
        <IconButton
          aria-label="Change Status"
          onClick={(e) => {
            console.log(product);
            changeProductStatus(product);
          }}
        >
          <ChangeCircleIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
};

export default Product_Status;
