import React, { useCallback, useEffect, useMemo, useState } from "react";
import "./../Profile/styles.css";
import firebase from "../Firebase/fire";
//for using icons from font awesome lib
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faMessage,
  faCog,
  faPen,
  faKey,
  faArrowCircleRight,
  faCancel,
} from "@fortawesome/free-solid-svg-icons";
import { useUserContext } from "../../context/userContext";
import emailjs from "emailjs-com";
import {
  Card,
  CardMedia,
  CardContent,
  Typography,
  CardActions,
  IconButton,
} from "@material-ui/core";
import useStyles from "./styles";
import DoNotDisturb from "@mui/icons-material/DoNotDisturb";

const ProfileStoreOwner = () => {
  const { user, forgotPassword, role, displayName, phone, custEmail } =
    useUserContext();

  const [selectedItem, setSelectedItem] = useState(0);
  const [products, setProducts] = useState([]);
  const [comments, setComments] = useState([]);
  const [PoProducts, setPoProducts] = useState([]);
  const [docid, setDocId] = useState("");
  const classes = useStyles();
  const [displayName_, setDisplayName] = useState(displayName);
  const [currentPhoneNumber, setCurPhnNum] = useState(phone);
  const [message, setmessage] = useState("");

  var storeArr = [];

  const handleLogout = () => {
    firebase.auth().signOut();
  };

  const db = firebase.firestore();
  const refPurchase = db.collection("purchase-order");

  const changeDiplayname = async (name) => {
    console.log(name);
    firebase
      .firestore()
      .collection("users")
      .doc(user.uid)
      .update({ displayName: name });
    setDisplayName(name);
  };

  const changePhoneNumber = async (name) => {
    console.log(name);
    firebase
      .firestore()
      .collection("users")
      .doc(user.uid)
      .update({ phone: name });
    setCurPhnNum(name);
  };

  const getUserInfo = async () => {
    const verfctnRef = firebase.firestore().collection(`users`);
    const snapshot = await verfctnRef.where("email", "==", user.email).get();

    if (snapshot.empty) {
      console.log("No matching documents.");
      return;
    }

    snapshot.forEach(function (doc) {
      const data = doc.data();

      storeArr.push(data);

      console.log(storeArr);
      console.log(data);
      console.log(data.displayName);
      setDisplayName([data.displayName]);
    });
  };

  function fetchSoldProducts() {
    refPurchase.onSnapshot((querySnapshot) => {
      const product_list = [];
      querySnapshot.forEach((doc) => {
        //console.log(doc.data())
        const docData = [];
        docData.push(doc.data());
        Array.from(docData).forEach((element) => {
          //console.log(element)

          const day = element.PurchaseDate.toDate()
            .toDateString()
            .substring(0, 3);
          const month = element.PurchaseDate.toDate()
            .toDateString()
            .substring(4, 7);
          const dayNum = element.PurchaseDate.toDate()
            .toDateString()
            .substring(8, 10);
          const date = dayNum + "/" + month + "/2022," + day;
          const cartItemsArray = element.cartItems;
          Array.from(cartItemsArray).forEach((key) => {
            //console.log(key)
            if (key.Product_Seller === displayName && key.PO_Status === "1") {
              product_list.push({
                Picture: key.Product_Picture,
                Name: key.Product_Name,
                Status: key.PO_Status,
                Product_Id: key.Product_Id,
                DocId: doc.id,
                Email: element.email,
                BuyerName: element.buyerName,
                Date: date,
              });
            }
          });
        });
      });
      setPoProducts(product_list);
    });
  }

  const changePOStatus = async (docid, productId, email, buyerName) => {
    const refCustomers = firebase.firestore().collection("purchase-order");

    refCustomers.get().then(function (querySnapshot) {
      querySnapshot.forEach(function (doc) {
        if (doc.id === docid) {
          const list1 = doc.data().cartItems;
          Array.from(list1).forEach((element) => {
            if (element.Product_Id === productId) {
              doc.ref.update({
                cartItems: {
                  PO_Status: "0",
                },
              });
            }
          });
        }
      });
    });

    emailjs.init("9qipjvqAhN_zEkeDW");
    emailjs.send("service_1pripqs", "template_8bcnu0g", {
      to_email: email,
      to_name: buyerName,
      general_message: "Your order was canceled.",
    });
  };

  const storeProducts = firebase
    .firestore()
    .collection("Products")
    .where("Product_Seller", "==", displayName);

  const getProducts = useCallback(() => {
    storeProducts.onSnapshot((querySnapshot) => {
      const items = [];
      querySnapshot.forEach((doc) => {
        items.push({ product: doc.data(), id: doc.id });
      });
      setProducts(items);
    });
  }, [products, storeProducts]);
  //console.log(products);
  const getAllComments = () => {
    const temp = [];

    if (!comments[0]) {
      for (var i = 0; i < products.length; i++) {
        var hayirlisi = firebase
          .firestore()
          .collection("Products/" + products[i].id + "/Comments&Ratings");

        const test = products[i].id;
        hayirlisi.onSnapshot((querySnapshot) => {
          querySnapshot.forEach((doc) => {
            temp.push({
              comment: doc.data(),
              id: doc.id,
              productId: test,
            });
          });
        });
      }

      setComments(temp);
    }
  };

  const handleApproveComment = (productId, commentId) => {
    firebase
      .firestore()
      .collection("Products")
      .doc(productId)
      .collection("Comments&Ratings")
      .doc(commentId)
      .update({ Comment_Status: 1 });
  };

  useEffect(() => {
    if (!products[0]) {
      getProducts();
    }
  }, [getProducts]);

  useEffect(() => {
    fetchSoldProducts();
  }, []);

  //console.log(PoProducts);

  const forgotPasswordHandler = (e) => {
    forgotPassword(custEmail);
    setmessage("Please, check your email box to change your password!");
  };

  return (
    <div class="container">
      <div className="leftbox">
        <nav>
          <a
            onClick={() => {
              setSelectedItem(0);
              getAllComments();
              console.log(selectedItem);
            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(5);
              console.log(selectedItem);
            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon
                icon={faPen}
                onClick={() => {
                  getUserInfo();
                }}
              ></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(1);
            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faCancel}></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(2);
              getAllComments();
              console.log(selectedItem);
              console.log(storeProducts);
            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faMessage}></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(4);
              console.log(selectedItem);
            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faCog}></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(3);
              console.log(selectedItem);
            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faKey}></FontAwesomeIcon>
            </span>
          </a>
        </nav>
      </div>
      <div className="rightbox">
        {selectedItem === 0 ? (
          <div>
            <h1>Company Info</h1>

            <h2>Full Name:</h2>
            <h3>{displayName_}</h3>

            <h2>Role:</h2>
            <h3>{role}</h3>

            <h2>Email:</h2>
            <h3>{custEmail}</h3>

            <h2>Phone Number:</h2>
            <h3>{currentPhoneNumber}</h3>
          </div>
        ) : selectedItem === 1 ? (
          <div>
            <h1>Cancel Order</h1>
            {PoProducts.map((product) => (
              <Card className={classes.root}>
                <CardMedia
                  className={classes.media}
                  image={product.Picture}
                  title={product.Name}
                />
                <CardContent>
                  <div className={classes.cardContent}>
                    <Typography variant="h5" component="h2" gutterBottom>
                      {product.Name}
                    </Typography>
                    <Typography variant="h5" component="h2" gutterBottom>
                      {product.Date}
                    </Typography>
                  </div>
                  <Typography
                    variant="body2"
                    color="textSecondary"
                    component="p"
                    gutterBottom
                  >
                    Status:{" "}
                    {product.Status === "1" ? (
                      <div>Approved</div>
                    ) : (
                      <div>0</div>
                    )}
                  </Typography>
                </CardContent>
                <CardActions disableSpacing className={classes.cardActions}>
                  <IconButton
                    aria-label="Change Status"
                    onClick={(e) => {
                      console.log(product.DocId);
                      changePOStatus(
                        product.DocId,
                        product.Product_Id,
                        product.Email,
                        product.buyerName
                      );
                    }}
                  >
                    <DoNotDisturb />
                    Cancel
                  </IconButton>
                </CardActions>
              </Card>
            ))}
            ;
          </div>
        ) : selectedItem === 2 ? (
          <div>
            <h1>Comments</h1>
            {comments.length === 0 ? (
              <h2>No Comment...</h2>
            ) : (
              <>
                {comments.map((comment, index) => (
                  <>
                    {comment.comment.Comment_Status === 0 && (
                      <div key={index}>
                        <span>{comment.comment.Comment}</span>

                        <button
                          onClick={() =>
                            handleApproveComment(comment.productId, comment.id)
                          }
                        >
                          Approve
                        </button>
                      </div>
                    )}
                  </>
                ))}
              </>
            )}
          </div>
        ) : selectedItem === 4 ? (
          <div>
            <h1>Settings</h1>

            <h2>logout</h2>
            <nav className="dolly">
              <FontAwesomeIcon icon={faArrowCircleRight}></FontAwesomeIcon>
            </nav>
          </div>
        ) : selectedItem === 5 ? (
          <div>
            <div>
              <form>
                <h2>Edit Store Name</h2>
                <Typography>Store Name: {displayName_}</Typography>
                <input
                  type="text"
                  required="required"
                  placeholder="Store Name"
                  onChange={(e) => {
                    changeDiplayname(e.target.value);
                  }}
                />
                <br />
                <br />
                <Typography>Phone Number: {currentPhoneNumber}</Typography>
                <input
                  type="text"
                  required="required"
                  placeholder="Your Phone Number"
                  onChange={(e) => {
                    changePhoneNumber(e.target.value);
                  }}
                />
                <br />
                <br />
              </form>
            </div>
          </div>
        ) : selectedItem === 3 ? (
          <div>
            <h1> CHANGE PASSWORD </h1>
            <h2>An e-mail will be sent to change your password. </h2>

            <div>
              <p onClick={forgotPasswordHandler}>
                <button id="button">Send</button>
              </p>
              <p> {message}</p>
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </div>
  );
};
export default ProfileStoreOwner;
