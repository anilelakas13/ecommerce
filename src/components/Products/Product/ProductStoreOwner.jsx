import { Card, CardMedia, CardContent, CardActions, Typography, IconButton} from '@material-ui/core';
import { Delete, Edit } from '@material-ui/icons';
import { Link } from 'react-router-dom';
import React from 'react';
import firebase from '../../Firebase/fire';
import useStyles from './styles';

const ProductStoreOwner = ({ product }) => {
    const classes = useStyles();
    
    const handleRemoveThisProduct = (product) => {
        firebase.firestore().collection("Products").where("Product_Id", "==", product.Product_Id).get()
        .then(querySnapshot => {
            querySnapshot.docs[0].ref.delete();
        });
    }

    return (
    <Card className={classes.root} >
        <CardMedia className={classes.media} image={product.Product_Picture} title={product.Product_Name} />
        <CardContent>
            <div className={classes.cardContent}>
                <Typography variant="h5" component="h2" gutterBottom>
                    {product.Product_Name}
                </Typography>
                <Typography variant="h5" component="h2" gutterBottom style={{ fontWeight: 600 }}>
                    {product.Product_Price} TL
                </Typography>
            </div>
            <Typography variant="body2" color="textSecondary" component="p" gutterBottom>
                    Stock: {product.Product_Available}
            </Typography>
        </CardContent>
        <CardActions disableSpacing className={classes.cardActions}>
            <Typography variant="body2" color="textPrimary" component="p" gutterBottom>
                Edit 
            </Typography>
            <IconButton aria-label="Edit product">
                <Link exact to ={'/EditProduct/'+product.Product_Id}>
                    <Edit />
                </Link>
            </IconButton>
            <Typography variant="body2" color="textPrimary" component="p" gutterBottom>
                
            </Typography>
            <IconButton aria-label="Remove product" onClick={() => handleRemoveThisProduct(product)}>
                <Delete />
            </IconButton>
        </CardActions>
    </Card>
  )
}

export default ProductStoreOwner
