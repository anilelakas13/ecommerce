import { Container, Typography } from '@material-ui/core';
import React from 'react'


function Customers({ customers }) {  
        return (                   
            <div>            
                <h1>Customers</h1>
                {customers.map((customer) => (
                    <div key={customer.displayName}>
                        <p>{customer.displayName}</p>
                        <p>{customer.phone}</p>
                        <p>{customer.email}</p>
                    </div>
                ))}        
            </div>      
    );
}

export default Customers;
