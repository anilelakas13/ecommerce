import { React, useState } from "react";
import { Grid, Typography } from "@material-ui/core";
import "./../Profile/styles.css";
import firebase from "../Firebase/fire";
import Product_Status from "./Product_Status";
import StoreOwner_Status from "./StoreOwner_Status";
//for using icons from font awesome lib
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faUser,
  faCheck,
  faKey,
  faDolly,
  faPen,
  faAddressCard,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { useUserContext } from "../../context/userContext";

const ProfileAdmin = ({
  tabSelection,
  allproducts,
  changeProductStatus,
  allStoreOwners,
  changeStoreOwnerStatus,
}) => {
  const { user, forgotPassword, role, displayName, phone, custEmail } =
    useUserContext();

  const [displayName_, setDisplayName] = useState(displayName);

  const [selectedItem, setSelectedItem] = useState(tabSelection);
  const [store_name, setStoreName] = useState([]);
  const [store_status, setStore_status] = useState([]);
  const [currentPhoneNumber, setCurPhnNum] = useState(phone);
  const [message, setmessage] = useState("");
  var storeArr = [];

  const [waitingStoreID, setWaitingStoreID] = useState("");

  const setStatus = async () => {
    firebase
      .firestore()
      .collection("users")
      .doc(waitingStoreID)
      .update({ status: 1 });
    setStore_status("1");
  };

  const changeDiplayname = async (name) => {
    console.log(name);
    firebase
      .firestore()
      .collection("users")
      .doc(user.uid)
      .update({ displayName: name });
    setDisplayName(name);
  };

  const changePhoneNumber = async (name) => {
    console.log(name);
    firebase
      .firestore()
      .collection("users")
      .doc(user.uid)
      .update({ phone: name });
    setCurPhnNum(name);
  };

  const verification = async () => {
    const verfctnRef = firebase.firestore().collection(`users`);
    const snapshot = await verfctnRef.where("status", "==", 0).get();

    if (snapshot.empty) {
      console.log("No matching documents.");
      return;
    }

    snapshot.forEach(function (doc) {
      const data = doc.data();

      storeArr.push(data);

      console.log(storeArr);
      console.log(data);
      console.log(data.displayName);
      setStoreName([data.displayName]);
      setStore_status([data.status]);
      setWaitingStoreID(snapshot.docs[snapshot.docs.length - 1].id);
    });
  };

  const getUserInfo = async () => {
    const verfctnRef = firebase.firestore().collection(`users`);
    const snapshot = await verfctnRef.where("email", "==", user.email).get();

    if (snapshot.empty) {
      console.log("No matching documents.");
      return;
    }

    snapshot.forEach(function (doc) {
      const data = doc.data();

      storeArr.push(data);

      console.log(storeArr);
      console.log(data);
      console.log(data.displayName);
      setDisplayName([data.displayName]);
    });
  };

  const forgotPasswordHandler = () => {
    forgotPassword(custEmail);
    setmessage("Please, check your email box to change your password!");
  };
  function refreshPage() {
    window.location.reload(false);
  }
  return (
    <div class="container">
      <div className="leftbox">
        <nav>
          <a
            onClick={() => {
              setSelectedItem(0);
            }}
            className="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faUser}></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(5);
              console.log(selectedItem);
            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon
                icon={faPen}
                onClick={() => {
                  getUserInfo();
                }}
              ></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(1);
            }}
            className="tab"
          >
            <span className="icons">
              <FontAwesomeIcon
                icon={faAddressCard}
                onClick={() => {
                  verification();
                }}
              ></FontAwesomeIcon>
            </span>
          </a>

          <a
            onClick={() => {
              setSelectedItem(2);
            }}
            className="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faCheck}></FontAwesomeIcon>
            </span>
          </a>

          <a
            onClick={() => {
              setSelectedItem(3);
              console.log(allproducts);
            }}
            className="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faDolly}></FontAwesomeIcon>
            </span>
          </a>
          <a
            onClick={() => {
              setSelectedItem(4);
              console.log(selectedItem);
            }}
            class="tab"
          >
            <span className="icons">
              <FontAwesomeIcon icon={faKey}></FontAwesomeIcon>
            </span>
          </a>
        </nav>
      </div>
      <div className="rightbox">
        {selectedItem === 0 ? (
          <div>
            <h1>Admin Info</h1>

            <h2>Full Name:</h2>
            <h3>{displayName_}</h3>
            <h2>Role:</h2>
            <h3>{role}</h3>
            <h2>Email Adress:</h2>
            <h3>{custEmail}</h3>
            <h2>Phone</h2>
            <h3>{currentPhoneNumber}</h3>
          </div>
        ) : selectedItem === 1 ? (
          <div>
            <h1>Accept/Reject a store creation application</h1>

            <h2>Store Owner Name</h2>
            <div>
              {" "}
              <table>
                <tbody>
                  {storeArr.map((s) => (
                    <tr key={s.email}>
                      <td> {s.displayName}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>

            <input type="text" class="input" value={store_name}></input>

            <h2>Store Status</h2>
            <input type="text" class="input" value={store_status}></input>

            <nav>
              <FontAwesomeIcon
                className="faCheck"
                icon={faCheck}
                onClick={() => {
                  setStatus();
                }}
              ></FontAwesomeIcon>

              <span></span>
              <FontAwesomeIcon
                className="faX"
                icon={faXmark}
                onClick={() => {
                  setStore_status("0");
                }}
              ></FontAwesomeIcon>
              <button onClick={refreshPage}>next</button>
            </nav>
          </div>
        ) : selectedItem === 2 ? (
          <div>
            <h1>Suspend/ Unsuspend Store</h1>

            <input type="text" class="input" value={store_name}></input>

            <Grid container spacing={4}>
              {allStoreOwners.map((store) => (
                <Grid
                  item
                  key={store.displayName}
                  xs={12}
                  sm={12}
                  md={12}
                  lg={12}
                >
                  <StoreOwner_Status
                    store={store}
                    changeStoreOwnerStatus={changeStoreOwnerStatus}
                    tabSelection={tabSelection}
                  />
                </Grid>
              ))}
            </Grid>
          </div>
        ) : selectedItem === 3 ? (
          <div>
            <h1>Suspend/ Unsuspend Product</h1>
            <h2>Products</h2>
            <Grid container spacing={4}>
              {allproducts.map((product) => (
                <Grid
                  item
                  key={product.Product_Name}
                  xs={4}
                  sm={4}
                  md={4}
                  lg={4}
                >
                  <Product_Status
                    product={product}
                    changeProductStatus={changeProductStatus}
                    tabSelection={tabSelection}
                  />
                </Grid>
              ))}
            </Grid>
          </div>
        ) : selectedItem === 5 ? (
          <div>
            <div>
              <form>
                <h2>Edit UserName</h2>

                <Typography>User Name: {displayName_}</Typography>
                <input
                  type="text"
                  required="required"
                  placeholder="User Name"
                  onChange={(e) => {
                    changeDiplayname(e.target.value);
                  }}
                />
                <br />
                <br />
                <Typography>Phone Number: {currentPhoneNumber}</Typography>
                <input
                  type="text"
                  required="required"
                  placeholder="Your Phone Number"
                  onChange={(e) => {
                    changePhoneNumber(e.target.value);
                  }}
                />
                <br />
                <br />
              </form>
            </div>
          </div>
        ) : selectedItem === 4 ? (
          <div>
            <h1> CHANGE PASSWORD </h1>
            <h2>An e-mail will be sent to change your password. </h2>

            <div>
              <p onClick={forgotPasswordHandler}>
                <button id="button">Send</button>
              </p>
              <p> {message}</p>
            </div>
          </div>
        ) : (
          <div></div>
        )}
      </div>
    </div>
  );
};
export default ProfileAdmin;
