import '../Checkout/styles.css';
import totalPrice from "../Cart/Cart";
import React, {Component } from 'react';
import {createPurchaseOrder} from '../Firebase/fire';
import { Typography } from "@material-ui/core";
import { useState } from "react";
import { ethers } from "ethers";
import ErrorMessage from "../Connector/ErrorMessage";
import TxList from "../Connector/TxList";
import Connector from "../Connector/Connector";
import {Link, useLocation} from "react-router-dom";
import emailjs from 'emailjs-com';



const initialState={
    email:'',
    AdresstoSend:'',
    Products:'',
    BuyerName:'',
    InvoiceAdress:'', 
    cartItems: null,
    totalAmount: 0,
};



class Checkout extends Component {
    constructor(props){
        super(props);
    }
    state=initialState;



    change = (e) => {
            const { name, value } = e.target;
            this.setState({ [name]: value });
    };
    
    complete = async (e) => {
            e.preventDefault();
            var ErrorMessage1 = "";
            var ErrorMessage2 = "";
            var ErrorMessage3 = "";
            var ErrorMessage4 = "";
            var ErrorMessage5 = "";
            var ErrorMessage6 = "";
            var ErrorMessage7 = "";
            var ErrorMessage8 = "";

            e.preventDefault();
            if (!this.state.email){
                ErrorMessage1 = "Invalid E-mail!";
            }

            if (this.state.BuyerName == ''|| this.state.BuyerName.includes('1','2','3','4','5','6','7','8','9','0','!','>','£','^','#','+','$','%','½','&','/','{','(','[',')',']','=','}','?','\"','*','-','|','_','>','.',':',';',',','+',"'")){
                ErrorMessage2 = "Invalid Name and Surname!";
            }

            if (!this.state.InvoiceAdress){
                ErrorMessage3= "Invalid Invoice Adress!";
            }
            if (!this.state.AdresstoSend){
                ErrorMessage4= "Invalid Adress to Send!";
            }
            /*
            if (!this.state.CreditCardNo){
                ErrorMessage5= "Invalid Credit Card Number!";
            }

            if (this.state.NameonCard ==''|| this.state.NameonCard.includes('1','2','3','4','5','6','7','8','9','0','!','>','£','^','#','+','$','%','½','&','/','{','(','[',')',']','=','}','?','\"','*','-','|','_','>','.',':',';',',','+',"'")){
                ErrorMessage6= "Invalid Name and Surname on Credit Card!";
            }

            if (!this.state.ExpireDate){
                ErrorMessage7= "Invalid Expired Date!";
            }

            if (!this.state.CVV){
                ErrorMessage8= "Invalid CVV!";
            }
            */
            this.setState({ErrorMessage1,ErrorMessage2,ErrorMessage3,ErrorMessage4});
            this.createPurchaseOrder()


        };
   
    getTotalAmount = () => {        
        const totalAmount = this.props.cartItems.reduce((price,item) => price + item.quantity * item.Product_Price, 0);
        return totalAmount;
    };
    
    createPurchaseOrder = async ()  => {
        console.log(this.state)
        if((this.state.ErrorMessage1 =="") && (this.state.ErrorMessage2 =="") && (this.state.ErrorMessage3 =="") && (this.state.ErrorMessage4 == "")){
            await createPurchaseOrder(
                this.state.email,
                this.state.BuyerName,
                this.state.InvoiceAdress,
                this.state.AdresstoSend,                
                this.state.cartItems,
                this.state.totalAmount
                )
                .then((docRef) => {
                    alert("Information Successfully Saved");
                })
                emailjs.init("9qipjvqAhN_zEkeDW");
                emailjs.send("service_1pripqs","template_etm44pv",{
                    to_email: this.state.email,
                    to_name: this.state.BuyerName
                    });
       
                emailjs.init("9qipjvqAhN_zEkeDW");
                emailjs.send("service_1pripqs","template_etm44pv",{
                    to_email: "anil.elakas@ozu.edu.tr",
                    to_name: "STORE OWNER"
                });       
                 
              
        }      
    };
    
    render() {
        const{setemail,setAdresstoSend,setBuyerName,setInvoiceAdress} = this.state;
        this.state.cartItems = this.props.cartItems;
        this.state.totalAmount = this.getTotalAmount();
        console.log(this.state);
        return(
            <div>
                <form className="Checkout"onSubmit={this.complete} >

                    <h2>Checkout</h2>
                    <h3> E-Mail for Invoice and Shipping Delivery Tracking </h3>
                        <input 
                            type= "email"
                            name= "email"
                            value={setemail}
                            onChange={this.change}
                            placeholder="Enter E-Mail Adress"
                            />
                        {this.state.ErrorMessage1 ? <div style={{fontSize:15, color:"Red"}}>{this.state.ErrorMessage1}</div> : <div></div>}
                        
                       <h3> Name and Surname for Invoice</h3>
                        <input 
                            type="text"
                            name= "BuyerName"
                            value={setBuyerName}
                            onChange={this.change}
                            placeholder="Enter Your Name and Surname Here"
                        />
                        {this.state.ErrorMessage2 ? <div style={{fontSize:15, color:"Red"}}>{this.state.ErrorMessage2}</div> : <div></div>}

                        <h3>Invoice Adress</h3>
                        <input 
                            type="name"
                            name= "InvoiceAdress"
                            value={setInvoiceAdress}
                            onChange={this.change}
                            placeholder="Enter Your Adress for Invoice"
                        />
                        {this.state.ErrorMessage3 ? <div style={{fontSize:15, color:"Red"}}>{this.state.ErrorMessage3}</div> : <div></div>}

                        <h3>Shipment Adress</h3>
                        <input 
                            type="name"
                            name= "AdresstoSend"
                            value={setAdresstoSend}
                            onChange={this.change}
                            placeholder="Enter Your Adress for Shipment"                            
                        />
                        {this.state.ErrorMessage4 ? <div style={{fontSize:15, color:"Red"}}>{this.state.ErrorMessage4}</div> : <div></div>}                        
                       
                        <Typography variant='h4' color="default" style={{fontSize:25, color:"Black", marginTop:20}} gutterBottom>
                            Total Price: {this.state.totalAmount} TL or 0.01 ETH 
                        </Typography>

                        <button className="Checkout button" onClick={this.complete}> Save My Information</button>                                                                        

                        <Link to="/Connector" params={{ to_email: "abc" }}>                            
                            <button className="Checkout button">Complete Payment</button>                            
                        </Link>
                            
                            
                            
                       

                       
                                                                                                                                                

                </form>   
            </div>
           );
    }
}

export default Checkout;