import React from "react";
import { Container, Button } from "@material-ui/core";
import useStyles from "./styles";
import { Link } from "react-router-dom";
import Stack from "@mui/material/Stack";
import { useState, useEffect } from "react";
import firebase from "../Firebase/fire";
import { Bar, Line, Pie, PolarArea } from "react-chartjs-2";
import Chart from "chart.js/auto";
import { useUserContext } from "../../context/userContext";

function AdminPage({ products }) {
  const classes = useStyles();
  const { user, role, displayName, userStatus, forgotPassword, logoutUser } =
    useUserContext();
  const [count, setCount] = useState([]);
  const [dayCount, setDayCount] = useState([]);
  const [monthCount, setMonthCount] = useState([]);
  const db = firebase.firestore();
  const refPurchase = db.collection("purchase-order");
  const [storeOwner, setStoreOwner] = useState("");
  const [productsLength, setProductsLength] = useState([]);

  const [dayData, setDayData] = useState({
    labels: [
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thursday",
      "Friday",
      "Saturday",
      "Sunday",
    ],
    datasets: [
      {
        label: "Sales by day",
        data: [],
        backgroundColor: [
          "red",
          "green",
          "red",
          "green",
          "red",
          "green",
          "red",
        ],
        borderColor: "black",
        borderWidth: "1",
      },
    ],
  });

  const [monthData, setMonthData] = useState({
    labels: [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    datasets: [
      {
        label: "Monthly comparison",
        data: [],
        backgroundColor: [
          "red",
          "yellow",
          "blue",
          "green",
          "grey",
          "red",
          "yellow",
          "blue",
          "green",
          "grey",
          "red",
          "yellow",
        ],
        borderColor: "black",
        borderWidth: "2",
      },
    ],
  });

  const [countData, setCountData] = useState({
    labels: ["Number of Products on Sale"],
    datasets: [
      {
        label: "Number of Products on Sale",
        data: [],
        backgroundColor: ["blue"],
        borderColor: "white",
        borderWidth: "2",
      },
    ],
  });

  const [lengthData, setLengthData] = useState({
    labels: ["Number of Products on Sale"],
    datasets: [
      {
        label: "Number of Products on Sale",
        data: [],
        backgroundColor: ["green"],
        borderColor: "white",
        borderWidth: "2",
      },
    ],
  });

  function fetchSoldProducts() {
    refPurchase.onSnapshot((querySnapshot) => {
      const items = [];
      querySnapshot.forEach((doc) => {
        items.push(doc.data());
      });
      const list1 = [];
      const list2 = [];
      const list3 = [];

      Array.from(items).forEach((element) => {
        const day = element.PurchaseDate.toDate()
          .toDateString()
          .substring(0, 3);
        const month = element.PurchaseDate.toDate()
          .toDateString()
          .substring(4, 7);
        list2.push(day);
        list3.push(month);
        const deneme2 = element.cartItems;
        Array.from(deneme2).forEach((key) => {
          list1.push({
            key: key.Product_Seller,
            value: key.Product_Category,
          });
        });
      });
      var dayoccurence = {
        Mon: 0,
        Tue: 0,
        Wed: 0,
        Thu: 0,
        Fri: 0,
        Sat: 0,
        Sun: 0,
      };
      list2.forEach((day) => {
        if (dayoccurence[day]) {
          dayoccurence[day] += 1;
        } else {
          dayoccurence[day] = 1;
        }
      });
      const dayCountArray = Object.values(dayoccurence);
      setDayCount(dayCountArray);

      var monthoccurence = {
        Jan: 0,
        Feb: 0,
        Mar: 0,
        Apr: 0,
        May: 0,
        Jun: 0,
        Jul: 0,
        Aug: 0,
        Sep: 0,
        Oct: 0,
        Nov: 0,
        Dec: 0,
      };
      list3.forEach((month) => {
        if (monthoccurence[month]) {
          monthoccurence[month] += 1;
        } else {
          monthoccurence[month] = 1;
        }
      });
      const monthCountArray = Object.values(monthoccurence);
      setMonthCount(monthCountArray);

      const cnt1 = [];
      cnt1.push(list1.length.toString());
      setCount(cnt1);
      const allProducts = [products.length];
      setProductsLength(allProducts);
    });
  }

  useEffect(() => {
    fetchSoldProducts();
  }, []);

  useEffect(() => {
    setCountData({
      labels: ["Number of products sold"],
      datasets: [
        {
          label: "Number of products sold",
          data: count,
          backgroundColor: ["blue"],
          borderColor: "white",
          borderWidth: "2",
        },
      ],
    });
  }, [count]);

  useEffect(() => {
    setDayData({
      labels: [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
      ],
      datasets: [
        {
          label: "Sales by day",
          data: dayCount,
          backgroundColor: [
            "gray",
            "yellow",
            "green",
            "blue",
            "red",
            "white",
            "black",
          ],
          borderColor: "black",
          borderWidth: "1",
        },
      ],
    });
  }, [dayCount]);

  useEffect(() => {
    setLengthData({
      labels: ["Number of products on Sale"],
      datasets: [
        {
          label: "Number of products on Sale",
          data: productsLength,
          backgroundColor: ["green"],
          borderColor: "white",
          borderWidth: "2",
        },
      ],
    });
  }, [productsLength]);

  useEffect(() => {
    setMonthData({
      labels: [
        "January",
        "February",
        "March",
        "April",
        "May",
        "June",
        "July",
        "August",
        "September",
        "October",
        "November",
        "December",
      ],
      datasets: [
        {
          label: "Monthly comparison",
          data: monthCount,
          backgroundColor: [
            "gray",
            "red",
            "blue",
            "yellow",
            "green",
            "white",
            "black",
            "pink",
            "purple",
            "brown",
            "red",
            "yellow",
          ],
          borderColor: "black",
          borderWidth: "2",
        },
      ],
    });
  }, [monthCount]);

  return (
    <Container>
      <div className={classes.toolbar} />
      <h2>Welcome {displayName}!</h2>
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "repeat(2, 1fr)",
          gridGap: 30,
        }}
      >
        <div>
          {" "}
          <Bar data={dayData}> </Bar>
        </div>
        <div>
          {" "}
          <Line data={dayData}> </Line>
        </div>
      </div>
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "repeat(3, 1fr)",
          gridGap: 20,
          marginTop: "50px",
        }}
      >
        <div>
          {" "}
          <PolarArea data={monthData}> </PolarArea>
        </div>
        <div>
          {" "}
          <Line data={monthData}> </Line>
        </div>
        <div>
          {" "}
          <Pie data={monthData}> </Pie>
        </div>
      </div>
      <div
        style={{
          display: "grid",
          gridTemplateColumns: "repeat(2, 1fr)",
          gridGap: 30,
          marginTop: "50px",
        }}
      >
        <div>
          {" "}
          <Bar data={countData}> </Bar>
        </div>
        <div>
          {" "}
          <Pie data={lengthData}> </Pie>
        </div>
      </div>
    </Container>
  );
}

export default AdminPage;
