import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { collection, doc, setDoc } from "firebase/firestore";  
import { getAuth } from "firebase/auth";

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyDZUJA-n6sWmQf3m19BfxXg7t_5zaf5Tig",
  authDomain: "e-commerce-b2485.firebaseapp.com",
  projectId: "e-commerce-b2485",
  storageBucket: "e-commerce-b2485.appspot.com",
  messagingSenderId: "9271827165",
  appId: "1:9271827165:web:747b94969d090e13b89934",
  measurementId: "G-VEF83S42D6"
};

const fire = firebase.initializeApp(firebaseConfig);
export const authorization = getAuth(fire);
export const auth = firebase.auth();
export const firestore = firebase.firestore();
export default fire;

export const createUserDocument = async (user, additionalData) => {
  if (!user) return;

  const userRef = firestore.doc(`users/${user.uid}`);

  const snapshot = await userRef.get();

  if (!snapshot.exists) {
    const { email } = user;
    const { name, role, phone } = additionalData;

    if(role === "Personal"){
      try {
        await userRef.set({
          status: 1,
          phone,
          role,
          displayName: name,
          email,
          createdAt: new Date(),
        });
      } catch (error) {

      }
    }else if(role === "Company"){

      try {
        await userRef.set({
          status: 0,
          phone,
          role,
          displayName: name,
          email,
          createdAt: new Date(),
        });
      } catch (error) {
        console.log('Error in creating user', error);
      }

    }else{
      console.log('Error in creating user about role value!');

    }
    
  }
  return userRef;
};

export const createPurchaseOrder = async (
  email, 
  buyerName, 
  invoiceAddress, 
  addressToSend, 
  cartItems,
  totalAmount,
  ) => {

  const db = fire.firestore();
  const purchaseOrderRef = collection(db, 'purchase-order');

  try {
    await setDoc(doc(purchaseOrderRef), {
      email, 
      buyerName, 
      invoiceAddress,
      addressToSend, 
      cartItems,
      totalAmount,
      ProductStatus:0,
      PurchaseDate: new Date()});
  } catch (error) {
    console.log('Error in purchase order', error);
  }
};
