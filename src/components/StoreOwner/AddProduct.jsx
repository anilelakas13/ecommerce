import React from "react";
import firebase from '../Firebase/fire';
import {useState, useEffect} from 'react';
  
const AddProduct = () => {
    const [productDiscount , SetDiscount] = useState("");
    const [productAvaliable , SetAvaliable] = useState("");
    const [productCategory , SetCategory] = useState("");
    const [productId , SetCounter] = useState("");
    const [productName  , SetName] = useState("");
    const [productPrice , SetPrice] = useState("");
    const [productSeller , SetSeller] = useState("");
    const [productPicture , SetPicture] = useState("");
    const db = firebase.firestore()
    const refCounter = db.collection("Counters")

    const sub = (e) => {
        e.preventDefault();       

        // Add data to the store
        db.collection("Products").add({
            Product_Discount: productDiscount,
            Product_Available: productAvaliable,
            Product_Category: productCategory,
            Product_Id: productId+1,
            Product_Name: productName,
            Product_Price: productPrice,
            Product_Seller: productSeller,
            Product_Picture: productPicture,
            Product_Status: "1"
        })
        .then((docRef) => {
            alert("Data Successfully Submitted");
        })
        .catch((error) => {
            console.error("Error adding document: ", error);
        });
        
        db.collection("Counters").doc('Product_Id').update({
          counterValue: productId+1
        })
    }

    function getProductId(){
      refCounter.onSnapshot((querySnapshot) => {
          const items = [];
          querySnapshot.forEach((doc) => {
              items.push(doc.data());              
          });
          SetCounter(items[0].counterValue);
      });
    }

    useEffect(() => {        
      getProductId();
    }, []); 


    return (
          
          <form justify="center" style={{marginTop:"100px", marginLeft:"800px"}}
            onSubmit={(event) => {sub(event)}}>
              <h2>Add Product</h2>
              <p>Product Name: </p>
              <input type="text" required="required" placeholder="Product Name"
                onChange={(e)=>{SetName(e.target.value)}} />
                <br/><br/>
              <p>Product Category: </p>
              <input type="text" required="required" placeholder="Product Category"
                onChange={(e)=>{SetCategory(e.target.value)}}/>
                <br/><br/>
              <p>Product Seller: </p>  
              <input type="text" required="required" placeholder="Product Seller"
                onChange={(e)=>{SetSeller(e.target.value)}}/>
                <br/><br/>                               
              <p>Product Price: </p>
              <input type="text" required="required" placeholder="Product Price"
                onChange={(e)=>{SetPrice(e.target.value)}}/>
                <br/><br/>
              <p>Product Stock: </p>
              <input type="text" required="required" placeholder="Product Stock"
                onChange={(e)=>{SetAvaliable(e.target.value)}}/>
                <br/><br/>
              <p>Product Discount: </p>
              <input type="text" required="required" placeholder="Product Discount"
                onChange={(e)=>{SetDiscount(e.target.value)}}/>
                <br/><br/>                  
              <p>Product Picture URL: </p>
              <input type="text" required="required" placeholder="Product Picture"
                onChange={(e)=>{SetPicture(e.target.value)}}/>
                <br/><br/>
              <button variant="contained" color="success" type="submit">Submit</button>
          </form>
    );
}
  
export default AddProduct;