import React from 'react'

const Cart = ({ cartItems, handleAddProduct, handleRemoveProduct }) => {
    const totalPrice = cartItems.reduce((price,item) => price + item.quantity * item.Product_Price, 0)
    return (
    <div>
        {cartItems.length === 0 && ( 
            <div>Cart is empty!</div>
        )}
        <div>
            {cartItems.map((item) => (
                <div key={item.Product_Id}>
                    <img
                    src={item.Product_Picture}
                    alt={item.Product_Name}
                    />
                    <div>{item.Product_Name}</div>
                    <div>
                        <button onClick={() => handleAddProduct}>+</button>
                        <button onClick={() => handleRemoveProduct}>-</button>
                    </div>
                    <div>{item.quantity} * {item.Product_Price} TL</div>
            </div>
            ))}
        </div>
        <div>
            Total Price
            <div>{totalPrice} TL</div>
        </div>
    </div>
  );
};


export default Cart;
