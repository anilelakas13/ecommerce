import React from "react";
import { Grid, Box } from "@material-ui/core";
import "./detailedstyles.css";
import Product from "./Product/Product";
import useStyles from "./styles";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import NativeSelect from "@mui/material/NativeSelect";
import { useNavigate } from "react-router-dom";

const Products = ({ products, handleAddProduct }) => {
  const classes = useStyles();
  const navigate = useNavigate();

  const handleChange = (event, chosenOption) => {
    console.log(event);
    console.log(event.target.value);

    if (event.target.value === "Suggested") {
      navigate("/Suggested");
    } else if (event.target.value === "Price Ascending") {
      navigate("/Price_Ascending");
      console.log(typeof products);
    } else if (event.target.value === "Price Descending") {
      navigate("/Price_Descending");
    } else if (event.target.value === "Rate Ascending") {
      navigate("/Rate_Ascending");
    } else if (event.target.value === "Rate Descending") {
      navigate("/Rate_Descending");
    }
  };
  return (
    <main className={classes.content}>
      <div className={classes.toolbar} />
      <div className="sortChoose">
        <Box sx={{ minWidth: 160 }}>
          <FormControl fullWidth>
            <InputLabel
              variant="standard"
              htmlFor="uncontrolled-native"
            ></InputLabel>
            <NativeSelect
              onChange={handleChange}
              defaultValue={"Suggested"}
              inputProps={{
                name: "chosenSort",
                id: "uncontrolled-native",
              }}
            >
              <option
                value={"Suggested"}
                onChange={(event) => handleChange(event, "Suggested")}
              >
                Suggested
              </option>
              <option
                value={"Price Ascending"}
                onChange={(event) => handleChange(event, "Price Ascending")}
              >
                Price Ascending
              </option>
              <option
                value={"Price Descending"}
                onChange={(event) => handleChange(event, "Price Descending")}
              >
                Price Descending
              </option>
              <option
                value={"Rate Ascending"}
                onChange={(event) => handleChange(event, "Rate Ascending")}
              >
                Rate Ascending
              </option>
              <option
                value={"Rate Descending"}
                onChange={(event) => handleChange(event, "Rate Descending")}
              >
                Rate Descending
              </option>
            </NativeSelect>
          </FormControl>
        </Box>
      </div>

      <Grid container justify="center" spacing={4}>
        {products.map((product) => (
          <Grid item key={product.Product_Name} xs={12} sm={6} md={3} lg={2}>
            <Product product={product} handleAddProduct={handleAddProduct} />
          </Grid>
        ))}
      </Grid>
    </main>
  );
};

export default Products;
