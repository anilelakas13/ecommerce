import React from "react";
import { Container, Typography, Button, Grid, Card } from "@material-ui/core";
import { Link } from "react-router-dom";
import useStyles from "./styles";
import CartItem from "./CartItem/CartItem";

const Cart = ({
  cartItems,
  handleAddProduct,
  handleRemoveProduct,
  handleClearCart,
}) => {
  const isEmpty = !cartItems?.length;
  const classes = useStyles();
  const totalPrice = cartItems.reduce(
    (price, item) => price + item.quantity * item.Product_Price,
    0
  );

  const EmptyCart = () => (
    <Typography variant="subtitle1">
      You have no items in your shopping cart!
      <Link to="/" className={classes.link}>
        {" "}
        Start adding some!
      </Link>
    </Typography>
  );

  const FilledCart = () => (
    <>
      <Grid container spacing={3}>
        {cartItems.map((item, index) => (
          <Grid item xs={12} sm={4} key={item.Product_Id + index}>
              <CartItem
                item={item}
                handleAddProduct={handleAddProduct}
                handleRemoveProduct={handleRemoveProduct}
              />
          </Grid>
        ))}
      </Grid>
      <div className={classes.cardDetails}>
        <Typography variant="h4">Subtotal: {totalPrice} TL</Typography>
        <div>
          <Button
            className={classes.emptyButton}
            size="large"
            type="button"
            variant="contained"
            color="secondary"
            onClick={handleClearCart}
          >
            Empty Cart
          </Button>
          <Button 
          className={classes.checkoutButton} 
          component={Link} 
          to={{ pathname: "/Checkout" }} 
          size="large"
          type="button" 
          variant="contained" 
          color="primary">
            Checkout
            </Button>
        </div>
      </div>
    </>
  );

  return (
    <Container>
      <div className={classes.toolbar} />
      <Typography className={classes.title} variant="h3" gutterBottom>
        Your Shopping Cart
      </Typography>
      {isEmpty ? <EmptyCart /> : <FilledCart />}
    </Container>
  );
};

export default Cart;
