import { createContext, useContext, useState } from "react";
import {
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
  onAuthStateChanged,
  signOut,
  updateProfile,
  sendPasswordResetEmail,
} from "firebase/auth";
import { authorization } from "../components/Firebase/fire";
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { useNavigate } from 'react-router-dom';

export const UserContext = createContext({});

export const useUserContext = () => {
  return useContext(UserContext);
};
export const firestore = firebase.firestore();

export const UserContextProvider = ({ children }) => {
  const [user, setUser] = useState(null);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");
  const [role, setRole] = useState("");
  const [displayName, setDisplayName] = useState("");
  const [userStatus, setUserStatus] = useState("");
  const [custEmail, setCustEmail] = useState("");
  const [phone, setPhone] = useState("");

  useState(() => {
    setLoading(true);
    const unsubscribe = onAuthStateChanged(authorization, (res) => {
      if (res) {
        setUser(res);
        console.log(res);
      } else {
        setUser(null);
      }
      setError("");
      setLoading(false);
    });
    return unsubscribe;
  }, []);

  const registerUser = (email, password, name, phone, role) => {
    setLoading(true);

    if(role == "Personal"){
      console.log("personal")
      createUserWithEmailAndPassword(authorization, email, password)
      .then(() =>
        updateProfile(authorization.currentUser, {
          status: 1,
          displayName: name,
          phone: phone,
          role: role,
          createdAt: new Date(),
        })
       
      )
      .then((res) => console.log(res))
      .catch((err) => setError(err.message))
      .finally(() => setLoading(false));

      console.log(authorization.currentUser);
    }
    else if(role === "Company"){
      console.log("Company")
      createUserWithEmailAndPassword(authorization, email, password)
      .then(() =>
        updateProfile(authorization.currentUser, {
          status: 0,
          displayName: name,
          phone: phone,
          role: role,
          createdAt: new Date(),
        })
      )
      .then((res) => console.log(res))
      .catch((err) => setError(err.message))
      .finally(() => setLoading(false));
      }
  };

  const signInUser = (email, password) => {
    setLoading(true);
    signInWithEmailAndPassword(authorization, email, password)
      .then((res) => console.log(res))
      .catch((err) => setError(err.code))
      .finally(() => setLoading(false));
  };

  const logoutUser = () => {
    signOut(authorization);
  };

  const forgotPassword = (email) => {
    return sendPasswordResetEmail(authorization, email);
  };

  const setUserRole = (role) => {
    return setRole(role);
  };

  const setUserName = (name) => {
    return setDisplayName(name);
  };

  const setStatus = (status) => {
    return setUserStatus(status);
  };

  const setUserEmail = (email) => {
    return setCustEmail(email);
  };
  
  const setPhoneNumber = (phone) => {
    return setPhone(phone);
  };
  


  const contextValue = {
    user,
    loading,
    error,
    role,
    displayName,
    userStatus,
    custEmail,
    phone,
    setUserEmail,
    setStatus,
    setUserName,
    setUserRole,
    signInUser,
    registerUser,
    logoutUser,
    forgotPassword,
    setPhoneNumber,
  };
  return (
    <UserContext.Provider value={contextValue}>{children}</UserContext.Provider>
  );
};