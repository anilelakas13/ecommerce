import React from 'react'
import { Card, CardMedia, CardContent, CardActions, Typography, IconButton} from '@material-ui/core';
import { AddShoppingCart } from '@material-ui/icons';
import { Link } from 'react-router-dom';


import useStyles from './styles';

const Product = ({ product, handleAddProduct}) => {
    const classes = useStyles();

    return (
    <Card className={classes.root}>
        <CardMedia className={classes.media} image={product.Product_Picture} title={product.Product_Name} />
        <CardContent>
            <div className={classes.cardContent}>
                <Typography variant="h5" component="h2" gutterBottom>
                    <Link to ={'/detailed_page/'+ product.Product_Id}>
                    {product.Product_Name}
                    </Link>
                </Typography>
                <Typography variant="h5" component="h2" gutterBottom style={{ fontWeight: 600 }}>
                    {product.Product_Price} TL
                </Typography>
            </div>
            <Typography variant="body2" color="textSecondary" component="p" gutterBottom>
                    Stock: {product.Product_Available}
            </Typography>
        </CardContent>
        <CardActions disableSpacing className={classes.cardActions}>
            <IconButton aria-label="Add to Cart" onClick={() => handleAddProduct(product)}>
                <AddShoppingCart/>
            </IconButton>
        </CardActions>
    </Card>
  )
}

export default Product
