import React from 'react';
import { MenuItem, Grid, Select, FormControl, InputLabel, FormHelperText, makeStyles,Container  } from '@material-ui/core';
import useStyles from './styles';
import Product from '../Products/Product/Product';

function Categories({ products, handleAddProduct }) {
  const classes = useStyles();

  const [value, setValue] = React.useState('');

  const handleChange = (event) => {
    setValue(event.target.value);
  };

  const filteredProducts = products.map((product) => 
    product.Product_Category).filter(function (value, index, array) { 
    return array.indexOf(value) === index;
  });

  return (

    <Container>
        <div className={classes.toolbar}/>
        <h2>Please select a category to see products</h2>
        <div className={classes.combobox}>
        <FormControl variant="standard" style={{marginTop:"100px" }} className={classes.FormControl}>
        <InputLabel shrink>Categories</InputLabel>
        <Select
            labelId="select-demo"
            id="demo-simple-select"
            displayEmpty
            value={value}
            onChange={handleChange}
        > 
        <MenuItem value={'all'}>All</MenuItem>
        {filteredProducts.map((product,index) => (
            <MenuItem item key={index} value={product}>{product}</MenuItem>))}
        </Select>
        <FormHelperText>You can select Category</FormHelperText>
        </FormControl>

        </div>
        <main className={classes.content}>
            <div className={classes.toolbar} />
            <Grid container justify="center" spacing={4}>
                {products.map((product) => (
                  <>
                  {product.Product_Category === value &&(
                    <Grid item key={product.Product_Category} xs={12} sm={6} md={3} lg={2}>
                      <Product product={product} handleAddProduct={handleAddProduct}/>
                    </Grid>
                    )}
                    {value === 'all' &&(
                    <Grid item key={product.Product_Category} xs={12} sm={6} md={3} lg={2}>
                      <Product product={product} handleAddProduct={handleAddProduct}/>
                    </Grid>
                    )}  
                    </>
                ))}
            </Grid>
        </main>
    </Container>
  );
}

export default Categories;