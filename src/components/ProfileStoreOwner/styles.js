import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(() => ({
  root: {
    MaxWidth: '100%',    
  },
  media: {
    height: 250,
    width:250,
    //paddingTop: '56.25%', // 16:9
  },
  cardActions: {
    display: 'flex',
    justifyContent: 'flex-end',
    paddingRight: '40px',
  },
  cardContent: {
    display: 'flex',
    justifyContent: 'space-between',
  },
}));