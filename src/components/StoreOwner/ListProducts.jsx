import React from 'react';
import { Grid, Container  } from '@material-ui/core';
import useStyles from './styles';
import ProductStoreOwner from '../Products/Product/ProductStoreOwner';
import { useUserContext } from "../../context/userContext";


function ListProducts({ products }) {
  const classes = useStyles();
  const { displayName } = useUserContext();

  return (

    <Container>
        <div className={classes.toolbar}/>
        <h2>Welcome {displayName}!</h2>
        <main className={classes.content}>
            <div className={classes.toolbar} />
            <Grid container justify="center" spacing={4} >
                {products.map((product) => (
                  <>
                  {product.Product_Seller === displayName &&(
                    <Grid item key={product.Product_Id} xs={12} sm={6} md={3} lg={2}>
                      <ProductStoreOwner product={product} />
                    </Grid>
                    )}
                  </>
                ))}
            </Grid>
        </main>
    </Container>
  );
}

export default ListProducts;