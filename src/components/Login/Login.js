import React, { useState, useEffect, useRef } from "react";
import fire, { auth } from "../Firebase/fire";
import "./styles.css";
import { Link } from "react-router-dom";
import { useUserContext } from "../../context/userContext";
import { message } from "antd";

import loginbackground from "../../assets/loginpage.jpg";

const LogIn = () => {
  const [selectedItem, setSelectedItem] = useState(0);

  const [email, setEmail] = useState("");
  const [role, setRole] = useState("");
  const [displayName, setDisplayName] = useState([]);
  const [userStatus, setUserStatus] = useState([]);
  const [password, setPassword] = useState("");
  const [CustEmail, setCustEmail] = useState("");
  const [UserPhone, setUserPhone] = useState("");
  const [emailError, setEmailError] = useState("");
  const [passwodError, setPasswordError] = useState("");

  const emailRef = useRef();
  const psdRef = useRef();

  const [loading, setLoading] = useState(false);
  const [islogged, setIsLogged] = useState(false);
  const [user, setUser] = useState([]);

  const {
    signInUser,
    forgotPassword,
    setUserRole,
    setUserEmail,
    setUserName,
    setStatus,
    setPhoneNumber,
  } = useUserContext();

  const resetForm = () => {
    setEmail("");
    setPassword("");
  };

  const resetError = () => {
    setEmailError("");
    setPasswordError("");
  };

  function getUser(refUser) {
    setLoading(true);
    refUser.onSnapshot((querySnapshot) => {
      const items = [];
      querySnapshot.forEach((doc) => {
        items.push(doc.data());
      });
      setUser(items);
      setDisplayName(items[0]?.displayName);
      setUserStatus(items[0]?.status);
      setRole(items[0]?.role);
      setCustEmail(items[0]?.email);
      setUserPhone(items[0]?.phone);
      setLoading(false);
    });
  }

  useEffect(() => {
    auth.onAuthStateChanged((authUser) => {
    });
  }, []);

  const handleSubmit = async (e) => {
    e.preventDefault();

    resetError();

    const email = emailRef.current.value;
    const password = psdRef.current.value;

    if (email && password) {
      signInUser(email, password);
    }

    const refUser = fire
      .firestore()
      .collection("users")
      .where("email", "==", email);
    getUser(refUser);

    try {
      await auth.signInWithEmailAndPassword(email, password).catch((err) => {
        switch (err.code) {
          case "auth/invalid-email":
          case "auth/user-disabled":
          case "auth/user-not-found":
            setEmailError(err.message);
            break;
          case "auth/wrong-password":
            setPasswordError(err.message);
            break;
        }
      });

      if (emailError === null && passwodError === null) {
        setIsLogged(true);
      }

      resetForm();
    } catch (error) {
      console.log("error logging in", error);
      message.error("Error ", error);
    }

    setSelectedItem(1);
  };

  if (loading) {
    return <h1>Loading...</h1>;
  }

  const forgotPasswordHandler = () => {
    const email = emailRef.current.value;
    if (email) {
      forgotPassword(email)
        .then(() => {
          emailRef.current.value = "";
        })
        .catch((err) => {
          switch (err.code) {
            case "auth/invalid-email":
            case "auth/user-disabled":
            case "auth/user-not-found":
              setEmailError(err.message);
              break;
            case "auth/wrong-password":
              setPasswordError(err.message);
              break;
          }
        });
    } else {
      setEmailError("Error - Email address is required!");
    }
  };

  setUserRole(role);
  setUserName(displayName);
  setStatus(userStatus);
  setUserEmail(CustEmail);
  setPhoneNumber(UserPhone);

  const myStyle = {
    backgroundImage: `url(${loginbackground})`,
    height: "100vh",
    backgroundSize: "cover",
    opacity: 1,
    backgroundRepeat: "no-repeat",
  };

  return (
    <div style={myStyle}>
      <form className="signup-login">
        <div class="login-form">
          <h1>LOGIN</h1>
          <input
            placeholder="Email"
            type="email"
            autoFocus
            required
            ref={emailRef}
          />
          <p className="errorMsg">{emailError}</p>
          <input placeholder="Password" type="password" ref={psdRef} />
          <p className="errorMsg">{passwodError}</p>
          <button onClick={handleSubmit} color="primary" class="px-4">
            LOGIN
          </button>
          <p onClick={forgotPasswordHandler}>Forgot Password?</p>
          <Link to="/register">Don't you have an account?</Link>
        </div>
      </form>
    </div>
  );
};

export default LogIn;
